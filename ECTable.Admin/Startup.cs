﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECTable.Admin.Startup))]
namespace ECTable.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
