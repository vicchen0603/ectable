﻿// 排序
// 參數說明：
// sortingField：要排序的欄位名稱
// currentSortingField：目前排序的欄位
// currentSortingDirection：目前排序的方式(Asc 或 Desc)
function changeSortingField(sortingField, currentSortingField, currentSortingDirection) {
    if (currentSortingField.toLowerCase() == sortingField.toLowerCase()) {
        switch (currentSortingDirection) {
            case 'None':
            case 'Desc':
                $('#sortingDirection').val('Asc');
                break;
            case 'Asc':
                $('#sortingDirection').val('Desc');
                break;
        }
    }
    else {
        $('#sortingDirection').val('Asc');
    }

    $('#sortingField').val(sortingField);
    $('#ListForm').submit();
};