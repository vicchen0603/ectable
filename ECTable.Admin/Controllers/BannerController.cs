﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// Banner 管理
    /// </summary>
    [SiteAuthorize()]
    public class BannerController : Controller
    {
        /// <summary>
        /// Banner 清單
        /// </summary>
        /// <returns></returns>
        public ActionResult BannerList()
        {
            return View();
        }
	}
}