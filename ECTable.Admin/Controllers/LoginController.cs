﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Admin.ViewModels;
using ECTable.Admin.Models;
using System.ComponentModel;
using ECTable.DataSource;
using MySql.Data.MySqlClient;
using ECTable.Utils;
using System.Data;
using ECTable.Utils.Security ;
using ECTable.Admin.Library;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 登入管理
    /// </summary>
    public class LoginController : Controller
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告

        /// <summary>
        /// 登入 - get
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            //清Session
            Session.Clear();
            return View();
        }

        /// <summary>
        /// 登入 - post
        /// </summary>
        /// <param name="uid">帳號</param>
        /// <param name="pwd">密碼</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(string uid, string pwd)
        {
            try
            {
                //清Session
                Session.Clear();
                MySqlParameter[] para = new MySqlParameter[1];
                para[0] = new MySqlParameter("Uid", uid);
                string sql = "select uid,login_password,IsUserPower,CreateTime " + Environment.NewLine;
                sql += " from manager where uid=@Uid and status=1 " + Environment.NewLine;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string userKeyPassword = SecurityPlus.encryptMd5(pwd + Convert.ToDateTime(dt.Rows[0]["CreateTime"]).ToString("yyyy-MM-ddTHH:mm:ss"));
                    if (userKeyPassword == dt.Rows[0]["login_password"].ToString())
                    {
                        Session["Uid"] = dt.Rows[0]["Uid"].ToString();
                        Session["IsUserPower"] = dt.Rows[0]["IsUserPower"].ToString();

                        MySqlParameter[] uptpara = new MySqlParameter[2];
                        sql = "update manager set login_ipaddress=@IpAddress,loginTime=now() where Uid=@Uid";
                        uptpara[0] = new MySqlParameter("Uid", uid);
                        uptpara[1] = new MySqlParameter("IpAddress", Request.UserHostAddress);
                        db.ExecuteNonQuery(CommandType.Text, sql, uptpara);
                        return RedirectToAction("ManagerList", "Manager");
                    }
                }

                ViewData["Message"] = "登入失敗，無此帳號或密碼";
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("登入 - post，發生錯誤：{0}", ex));
            }

            return View();
        }
        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Clear();
            return View();
        }
	}
}