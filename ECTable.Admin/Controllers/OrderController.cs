﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 訂單管理
    /// </summary>
    [SiteAuthorize()]
    public class OrderController : Controller
    {
        /// <summary>
        /// 訂單清單
        /// </summary>
        /// <returns></returns>
        public ActionResult OrderList()
        {
            return View();
        }
	}
}