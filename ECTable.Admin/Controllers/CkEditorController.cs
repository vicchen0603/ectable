﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Utils.UploadFiles;
using System.IO;

namespace ECTable.Admin.Controllers
{
    public class CkEditorController : Controller
    {
        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["CkEditorFilePath"];
            //如果沒這資料夾就建這資料夾

            if (!Directory.Exists(Server.MapPath("~/" + filePath))) Directory.CreateDirectory(Server.MapPath("~/" + filePath));
            string result = "";
            if (upload != null && upload.ContentLength > 0)
            {
                
                //儲存圖片至Server

                string fileName = Path.GetFileName(upload.FileName);
                string saveFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + fileName;
                upload.SaveAs(Server.MapPath("~/" + filePath + "/" + saveFileName));


                var imageUrl = Url.Content("~/" + filePath + "/" + saveFileName);

                var vMessage = string.Empty;

                result = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imageUrl + "\", \"" + vMessage + "\");</script></body></html>";

            }

            return Content(result);
        }
	}
}