﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 會員管理
    /// </summary>
    [SiteAuthorize()]
    public class MemberController : Controller
    {
        /// <summary>
        /// 會員清單
        /// </summary>
        /// <returns></returns>
        public ActionResult MemberList()
        {
            return View();
        }
	}
}