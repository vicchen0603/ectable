﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 策展管理
    /// </summary>
    [SiteAuthorize()]
    public class CurationController : Controller
    {
        /// <summary>
        /// 策展清單
        /// </summary>
        /// <returns></returns>
        public ActionResult CurationList()
        {
            return View();
        }

        /// <summary>
        /// 策展管理 - 新增
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCuration()
        {
            return View();
        }

        /// <summary>
        /// 策展管理 - 編輯
        /// </summary>
        /// <returns></returns>
        public ActionResult EditCuration()
        {
            return View();
        }

        /// <summary>
        /// 策展管理 - 刪除
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteCuration()
        {
            return RedirectToAction("CurationList");
        }
	}
}