﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Admin.ViewModels;
using ECTable.Admin.Models;
using System.ComponentModel;
using ECTable.DataSource;
using MySql.Data.MySqlClient;
using ECTable.Utils;
using System.Data;
using PagedList;
using Filters;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 帳號管理
    /// </summary>
    [SiteAuthorize()]
    public class ManagerController : Controller
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        
        /// <summary>
        /// 帳號管理 - 列表
        /// </summary>
        /// <param name="pageNo">目前頁數</param>
        /// <returns></returns>
        public ActionResult ManagerList(ManagerViewModel viewModel, [DefaultValue(1)]int pageNo,
            ManagerSortingField sortingField = ManagerSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                int n = 0;
                if (Convert.ToInt32(Session["IsUserPower"]) != 1)
                {
                    n = 1;
                }

                MySqlParameter[] para = new MySqlParameter[n];
                //// 取出資料            
                string sql = "select id,uid,Login_IpAddress,case when LoginTime is null then '未登入' else LoginTime end LoginTime,status from manager where status<>2 " + Environment.NewLine;
                string isAdd = "style='display:block;'";
                if (Convert.ToInt32(Session["IsUserPower"]) != 1)
                {
                    isAdd = "style=display:none;";
                    sql += " where Uid=@Uid";
                    para[0] = new MySqlParameter("Uid", Session["Uid"].ToString());
                }
                viewModel.IsAdd = isAdd;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IQueryable<ManagerItem> queryList = (from a in dt.AsEnumerable()
                                                         select new ManagerItem()
                                                         {
                                                             Id = a["Id"].ToString(),
                                                             UID = a["Uid"].ToString(),
                                                             Status = Convert.ToSByte(a["Status"].ToString()),
                                                             Login_IpAddress = a["Login_IpAddress"].ToString(),
                                                             LoginTime = a["LoginTime"].ToString()
                                                         }).AsQueryable();

                    #region 排序處理
                    switch (viewModel.SortingField)
                    {
                        case ManagerSortingField.Uid:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.UID);
                            else
                                queryList = queryList.OrderBy(t => t.UID);
                            break;
                        case ManagerSortingField.LoginIpAddress:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Login_IpAddress);
                            else
                                queryList = queryList.OrderBy(t => t.Login_IpAddress);
                            break;
                        case ManagerSortingField.LoginTime:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.LoginTime);
                            else
                                queryList = queryList.OrderBy(t => t.LoginTime);
                            break;
                        case ManagerSortingField.Status:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Status);
                            else
                                queryList = queryList.OrderBy(t => t.Status);
                            break;
                        default:
                            viewModel.SortingField = ManagerSortingField.None;
                            viewModel.SortingDirection = SortingDirection.None;
                            break;
                    }
                    #endregion

                    // 分頁
                    viewModel.PageNo = pageNo;
                    viewModel.QueryList = queryList.ToPagedList(viewModel.PageNo, viewModel.PageSize);
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("登入管理 - 列表，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 帳號管理 - 新增 - get
        /// </summary>
        /// <returns></returns>
        public ActionResult AddManager()
        {
            ManagerEditViewModel viewModel = new ManagerEditViewModel();
            
            try
            {
                if (Convert.ToInt32(Session["IsUserPower"]) != 1)
                {
                    ViewData["Message"] = "你的帳號沒有權限新增";
                }

                viewModel.Status = 1;
                viewModel.IsUserPower = 0;
                viewModel.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("帳號管理 - 新增 - get，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 帳號管理 - 新增 - post
        /// </summary>
        /// <param name="viewModel">ManagerEditViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddManager(ManagerEditViewModel viewModel)
        {
            try
            {
                int id = GetMaxID();
                DateTime createTime = Convert.ToDateTime(viewModel.CreateTime);
                MySqlParameter[] para = new MySqlParameter[7];
                string sql = "insert into manager(id,Uid,Login_Password,IsUserPower,Status,CreateID,CreateTime)values(@id,@Uid,@Login_Password,@IsUserPower,@Status,@CreateID,@CreateTime)";
                string pwd = Utils.Security.SecurityPlus.encryptMd5(viewModel.Pwd + createTime.ToString("yyyy-MM-ddTHH:mm:ss"));

                para[0] = new MySqlParameter("CreateID", Session["Uid"].ToString());
                para[1] = new MySqlParameter("Uid", viewModel.UID);
                para[2] = new MySqlParameter("Login_Password", pwd);
                para[3] = new MySqlParameter("IsUserPower", viewModel.IsUserPower);
                para[4] = new MySqlParameter("Status", viewModel.Status);
                para[5] = new MySqlParameter("CreateTime", createTime);
                para[6] = new MySqlParameter("id", id);

                int act = db.ExecuteNonQuery(CommandType.Text, sql, para);
                if (act == 0)
                {
                    ViewData["Message"] = "新增失敗！";
                }
                else
                {
                    ViewData["Message"] = "新增成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("新增失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("帳號管理 - 新增 - post，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 取得 manager 的最新的 id
        /// </summary>
        /// <returns>MaxID</returns>
        public int GetMaxID()
        {
            int intValue = 0;
            string strValue = "0";

            try
            {
                string sql = "select max(id)+1 from manager";
                strValue = StringTool.IIfCheckDBNull(db.ExecuteScalar(CommandType.Text, sql, null)).ToString();
                if (strValue == "") strValue = "1";
                intValue = int.Parse(strValue);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("取得 manager 的最新的 id，發生錯誤：{0}", ex));
            }

            return intValue;
        }

        /// <summary>
        /// 驗證帳號
        /// </summary>
        /// <param name="uid">帳號編號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <returns></returns>
        [HttpPost]
        public int CheckManagerUid([DefaultValue("")]string uid, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize)
        {
            try
            {
                MySqlParameter[] para = new MySqlParameter[1];
                string sql = "select uid from manager where Uid=@Uid" + Environment.NewLine;
                para[0] = new MySqlParameter("Uid", uid);
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("驗證帳號，發生錯誤：{0}", ex));
                return 0;
            }
        }

        /// <summary>
        /// 帳號管理 - 編輯 - get
        /// </summary>
        /// <param name="uid">帳號編號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <returns></returns>
        public ActionResult EditManager(string uid, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize)
        {
            ManagerEditViewModel viewModel = new ManagerEditViewModel();
            
            try
            {
                if (Convert.ToInt32(Session["IsUserPower"]) != 1)
                {
                    if (Session["Uid"].ToString() != uid)
                    {
                        ViewData["Message"] = "你的帳號沒有權限可更新別人的資料";
                    }

                }
                if (ViewData["Message"] == null)
                {
                    MySqlParameter[] para = new MySqlParameter[1];
                    string sql = "select id,uid,IsUserPower,Status from manager where Uid=@Uid";
                    para[0] = new MySqlParameter("Uid", uid);
                    DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        viewModel.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                        viewModel.Status = Convert.ToSByte(dt.Rows[0]["Status"].ToString());
                        viewModel.IsUserPower = Convert.ToSByte(dt.Rows[0]["IsUserPower"].ToString());
                        viewModel.UID = dt.Rows[0]["Uid"].ToString();
                        viewModel.UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else
                    {
                        ViewData["Message"] = "無此帳號";
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("帳號管理 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["PageSize"] = pageSize;

            return View(viewModel);
        }

        /// <summary>
        /// 帳號管理 - 編輯 - post
        /// </summary>
        /// <param name="viewModel">ManagerEditViewModel</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditManager(ManagerEditViewModel viewModel ,[DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize)
        {
            try
            {
                int n = 3;
                string pwdMd5 = "";
                if (viewModel.Pwd != null)
                {
                    pwdMd5 = Utils.Security.SecurityPlus.encryptMd5(viewModel.Pwd + Convert.ToDateTime(viewModel.CreateTime).ToString("yyyy-MM-ddTHH:mm:ss"));
                    n += 1;
                }
                if (Convert.ToSByte(Session["IsUserPower"]) == 1)
                {
                    n += 2;
                }
                MySqlParameter[] para = new MySqlParameter[n];
                para[0] = new MySqlParameter("CreateID", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", viewModel.Id);
                para[2] = new MySqlParameter("UpdateID", Session["Uid"].ToString());
                string sql = "update manager set UpdateID=@UpdateID,UpdateTime=now()";
                n = 2;
                if (Convert.ToSByte(Session["IsUserPower"]) == 1)
                {
                    sql += ",Status=@Status,IsUserPower=@IsUserPower";
                    n += 1;
                    para[n] = new MySqlParameter("IsUserPower", viewModel.IsUserPower);
                    n += 1;
                    para[n] = new MySqlParameter("Status", viewModel.Status);
                }
                if (pwdMd5 != "")
                {
                    sql += ",Login_Password=@pwdMd5";
                    n += 1;
                    para[n] = new MySqlParameter("pwdMd5", pwdMd5);
                }
                sql += " where Id=@Id";
                int act = db.ExecuteNonQuery(CommandType.Text, sql, para);
                if (act == 0)
                {
                    ViewData["Message"] = "編輯失敗！";
                }
                else
                {
                    ViewData["Message"] = "編輯成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("帳號管理 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["PageSize"] = pageSize;

            return View(viewModel);
        }

        /// <summary>
        /// 帳號管理 - 狀態變更
        /// </summary>
        /// <param name="id">編號</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public int EditManagerStatus([DefaultValue(0)]int id, [DefaultValue(0)]int status)
        {
            int act = 0;

            try
            {
                MySqlParameter[] para = new MySqlParameter[3];
                string sql = "update manager set status=@status,updateid=@updateid,updatetime=now() where id=@id";
                para[0] = new MySqlParameter("updateid", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", id);
                para[2] = new MySqlParameter("status", status);
                act = db.ExecuteNonQuery(CommandType.Text, sql, para);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("商品清單 - 狀態變更，發生錯誤：{0}", ex));
            }

            return act;
        }
	}
}