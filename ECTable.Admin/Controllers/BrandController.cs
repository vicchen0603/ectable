﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Admin.ViewModels;
using ECTable.Admin.Models;
using System.ComponentModel;
using ECTable.DataSource;
using MySql.Data.MySqlClient;
using ECTable.Utils;
using System.Data;
using PagedList;
using Filters;
using ECTable.Utils.UploadFiles;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 品牌管理
    /// </summary>
    [SiteAuthorize()]
    public class BrandController : Controller
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        /// <summary>
        /// 品牌清單
        /// </summary>
        /// <returns></returns>
        public ActionResult BrandList(BrandViewModel viewModel, [DefaultValue(1)]int pageNo,
          [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
          BrandSortingField sortingField = BrandSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                int n = 0;
                if (queryTitle != "")
                {
                    n += 1;
                }
                if (queryStatus != -1)
                {
                    n += 1;
                }
                //// 取出資料
                MySqlParameter[] para = new MySqlParameter[n];
                //string sql = "select c.Id, c.Title,c.Status,c.Sort,c.UpdateTime " + Environment.NewLine;
                //sql += ",ifnull((select count(distinct p.id) as Total from products p where p.status<>2 and brand_id=c.id group by p.class_id),0) as Quantity " + Environment.NewLine;
                //sql += "from products_brand c where c.status<>2 " + Environment.NewLine;
                string sql = "select c.Id, c.Title,c.Status,c.Sort,c.UpdateTime " + Environment.NewLine;
                sql += ",ifnull((select count(distinct p.id) as Total from products p where p.status<>2 and brand_id=c.id group by p.brand_id),0) as Quantity " + Environment.NewLine;
                sql += "from products_brand c where c.status<>2 " + Environment.NewLine;
                n = 0;
                if (queryTitle != "")
                {
                    sql += " and c.Title like Concat('%' ,@Title,'%')" + Environment.NewLine;
                    para[n] = new MySqlParameter("Title", queryTitle);
                    n += 1;
                }
                if (queryStatus != -1)
                {
                    sql += " and c.Status=@Status" + Environment.NewLine;
                    para[n] = new MySqlParameter("Status", queryStatus);
                }
                sql += " order by c.updatetime desc, c.createtime desc" + Environment.NewLine;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IQueryable<BrandItem> queryList = (from a in dt.AsEnumerable()
                                                       select new BrandItem()
                                                       {
                                                           Id = Convert.ToInt32(a["Id"].ToString()),
                                                           Title = a["Title"].ToString(),
                                                           Status = Convert.ToSByte(a["Status"].ToString()),
                                                           Sort = Convert.ToInt32(a["Sort"].ToString()),
                                                           Quantity = Convert.ToInt32(a["Quantity"].ToString()),
                                                           UpdateTime = DateTime.Parse(a["UpdateTime"].ToString()).ToString("yyyy-MM-dd")
                                                       }).AsQueryable();

                    #region 排序處理
                    switch (viewModel.SortingField)
                    {
                        case BrandSortingField.Id:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Id);
                            else
                                queryList = queryList.OrderBy(t => t.Id);
                            break;
                        case BrandSortingField.Title:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Title);
                            else
                                queryList = queryList.OrderBy(t => t.Title);
                            break;
                        case BrandSortingField.Quantity:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Quantity);
                            else
                                queryList = queryList.OrderBy(t => t.Quantity);
                            break;
                        case BrandSortingField.Status:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Status);
                            else
                                queryList = queryList.OrderBy(t => t.Status);
                            break;
                        case BrandSortingField.Sort:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Sort);
                            else
                                queryList = queryList.OrderBy(t => t.Sort);
                            break;
                        case BrandSortingField.UpdateTime:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Sort);
                            else
                                queryList = queryList.OrderBy(t => t.Sort);
                            break;
                        default:
                            viewModel.SortingField = BrandSortingField.None;
                            viewModel.SortingDirection = SortingDirection.None;
                            break;
                    }
                    #endregion

                    // 分頁
                    viewModel.PageNo = pageNo;
                    viewModel.QueryList = queryList.ToPagedList(viewModel.PageNo, viewModel.PageSize);
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("品牌清單，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }


        /// <summary>
        /// 品牌管理 - 新增 - get
        /// </summary>
        /// <returns></returns>
        public ActionResult AddBrand()
        {
            BrandEditViewModel viewModel = new BrandEditViewModel();

            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            viewModel.FileSizeLimit = fileSizeLimit.ToString();
            
            try
            {
                viewModel.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                viewModel.Status = 1;
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("品牌管理 - 新增 - get，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 品牌管理 - 新增 - post
        /// </summary>
        /// <param name="viewModel">BrandEditViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddBrand(BrandEditViewModel viewModel)
        {
            string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFilePath"] + "/Brand/";
            string fileExtension = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileExtension"];
            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            string img_path;
            string img_name;

            viewModel.FileSizeLimit = fileSizeLimit.ToString();

            try
            {
                //檔案判斷
                string FileErrMsg = FileTool.chkOneFiles(viewModel.uploadFiles, fileExtension, fileSizeLimit);

                if (FileErrMsg == null)
                {
                    //上傳檔案
                    Utils.Models.FilesModels filesModels = new Utils.Models.FilesModels();
                    filesModels = FileTool.uploadOneFile(viewModel.uploadFiles, Server.MapPath(filePath), filePath);
                    img_name = filesModels.FileName;
                    img_path = filesModels.FilePath;
                    MySqlParameter[] para = new MySqlParameter[10];
                    para[0] = new MySqlParameter("title", viewModel.Title);
                    para[1] = new MySqlParameter("img_path", img_path);
                    para[2] = new MySqlParameter("img_name", img_name);
                    para[3] = new MySqlParameter("status", viewModel.Status);
                    para[4] = new MySqlParameter("sort", viewModel.Sort);
                    para[5] = new MySqlParameter("seokeyword", viewModel.SeoKeyword);
                    para[6] = new MySqlParameter("seodesc", viewModel.SeoDesc);
                    para[7] = new MySqlParameter("content", viewModel.Content);
                    para[8] = new MySqlParameter("createid", Session["Uid"].ToString());
                    para[9] = new MySqlParameter("updateid", Session["Uid"].ToString());
                    string sql = "insert into products_brand(" + Environment.NewLine;
                    sql += "title,img_path,img_name, status, sort,seokeyword,seodesc,content, createid, createtime, updateid, updatetime" + Environment.NewLine;
                    sql += ") values " + Environment.NewLine;
                    sql += "(" + Environment.NewLine;
                    sql += "@title,@img_path,@img_name, @status, @sort,@seokeyword,@seodesc,@content, @createid, now(), @updateid, now()" + Environment.NewLine;
                    sql += ")" + Environment.NewLine;

                    if (db.ExecuteNonQuery(CommandType.Text, sql, para) == 0)
                        ViewData["Message"] = "新增失敗！";
                    else
                    {
                        ViewData["Message"] = "新增成功！";
                    }
                }
                else
                {

                    ViewData["Message"] = FileErrMsg;
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("新增失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("品牌管理 - 新增 - post，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 品牌管理 - 編輯 - get
        /// </summary>
        /// <param name="id">流水號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="queryTitle">搜尋關鍵字</param>
        /// <param name="queryStatus">狀態</param>
        /// <param name="parameter">查詢過濾資訊</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        public ActionResult EditBrand(int? id, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize,
            [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
            BrandSortingField sortingField = BrandSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            BrandEditViewModel viewModel = new BrandEditViewModel();

            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            viewModel.FileSizeLimit = fileSizeLimit.ToString();

            try
            {
                MySqlParameter[] para = new MySqlParameter[1];
                para[0] = new MySqlParameter("id", id);

                string sql = "select * from products_brand where id=@id and status<>2";
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                if (dt != null && dt.Rows.Count > 0)
                {
                    viewModel.Id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    viewModel.UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    viewModel.Title = dt.Rows[0]["title"].ToString();
                    viewModel.ExistFile = dt.Rows[0]["img_path"].ToString() + dt.Rows[0]["img_name"].ToString();
                    viewModel.Status = Convert.ToSByte(dt.Rows[0]["status"].ToString());
                    viewModel.SeoKeyword = dt.Rows[0]["SeoKeyword"].ToString();
                    viewModel.SeoDesc = dt.Rows[0]["SeoDesc"].ToString();
                    viewModel.Content = dt.Rows[0]["Content"].ToString();
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("品牌管理 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["QueryTitle"] = queryTitle;
            ViewData["QueryStatus"] = queryStatus;

            return View(viewModel);
        }

        /// <summary>
        /// 品牌管理 - 編輯 - post
        /// </summary>
        /// <param name="id">流水號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="queryTitle">搜尋關鍵字</param>
        /// <param name="queryStatus">狀態</param>
        /// <param name="parameter">查詢過濾資訊</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditBrand(BrandEditViewModel viewModel, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize,
            [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
            BrandSortingField sortingField = BrandSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFilePath"] + "/Brand/";
            string fileExtension = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileExtension"];
            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            string img_path="";
            string img_name = "";

            viewModel.FileSizeLimit = fileSizeLimit.ToString();

            try
            {
                //檔案判斷
                string FileErrMsg = FileTool.chkOneFiles(viewModel.uploadFiles, fileExtension, fileSizeLimit);

                if (FileErrMsg != null)
                {
                    if (FileErrMsg.Contains("請上傳檔案") == false)
                    {
                        ViewData["Message"] = FileErrMsg;
                    }
                }
                int n = 8;
                if (ViewData["Message"] == null)
                {
                    if (FileErrMsg == null)
                    {
                        //上傳檔案
                        Utils.Models.FilesModels filesModels = new Utils.Models.FilesModels();
                        filesModels = FileTool.uploadOneFile(viewModel.uploadFiles, Server.MapPath(filePath), filePath);
                        img_name = filesModels.FileName;
                        img_path = filesModels.FilePath;
                        n = n + 2;
                    }

                    MySqlParameter[] para = new MySqlParameter[n];
                    para[0] = new MySqlParameter("id", viewModel.Id);
                    para[1] = new MySqlParameter("title", viewModel.Title);
                    para[2] = new MySqlParameter("status", viewModel.Status);
                    para[3] = new MySqlParameter("sort", viewModel.Sort);
                    para[4] = new MySqlParameter("seokeyword", viewModel.SeoKeyword);
                    para[5] = new MySqlParameter("seodesc", viewModel.SeoDesc);
                    para[6] = new MySqlParameter("content", viewModel.Content);
                    para[7] = new MySqlParameter("updateid", Session["Uid"].ToString());
                    if (FileErrMsg == null)
                    {
                        para[8] = new MySqlParameter("img_path", img_path);
                        para[9] = new MySqlParameter("img_name", img_name);
                    }

                    string sql = "update products_brand set " + Environment.NewLine;
                    sql += "title=@title" + Environment.NewLine;
                    if (FileErrMsg == null)
                    {
                        sql += ",img_path=@img_path" + Environment.NewLine;
                        sql += ",img_name=@img_name" + Environment.NewLine;
                    }
                    sql += ",status=@status" + Environment.NewLine;
                    sql += ",sort=@sort" + Environment.NewLine;
                    sql += ",seokeyword=@seokeyword" + Environment.NewLine;
                    sql += ",seodesc=@seodesc" + Environment.NewLine;
                    sql += ",content=@content" + Environment.NewLine;
                    sql += ",updateid=@updateid" + Environment.NewLine;
                    sql += ",updatetime=now()" + Environment.NewLine;
                    sql += "where id=@id" + Environment.NewLine;

                    if (db.ExecuteNonQuery(CommandType.Text, sql, para) == 0)
                        ViewData["Message"] = "編輯失敗！";
                    else
                    {
                        ViewData["Message"] = "編輯成功！";
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("品牌管理 - 編輯 - post，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["QueryTitle"] = queryTitle;
            ViewData["QueryStatus"] = queryStatus;

            return View(viewModel);
        }


        /// <summary>
        /// 品牌管理 - 狀態變更
        /// </summary>
        /// <param name="id">主key</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public int EditBrandStatus([DefaultValue(0)]int id, [DefaultValue(0)]int status)
        {
            int act = 0;

            try
            {
                MySqlParameter[] para = new MySqlParameter[3];
                string sql = "update products_brand set Status=@Status,Updateid=@UId,UpdateTime=now() where id=@id";
                para[0] = new MySqlParameter("Uid", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", id);
                para[2] = new MySqlParameter("Status", status);
                act = db.ExecuteNonQuery(CommandType.Text, sql, para);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("品牌管理 - 狀態變更，發生錯誤：{0}", ex));
            }
            
            return act;
        }
	}
}