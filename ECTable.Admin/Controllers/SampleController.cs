﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Utils.UploadFiles;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace ECTable.Admin.Controllers
{
    public class SampleController : Controller
    {
        // GET: Sample
        public ActionResult Index()
        {     
            return View();
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> uploadFiles)
        {
            string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFilePath"];
            string fileExtension = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileExtension"];
            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            //檔案判斷
            string FileErrMsg = FileTool.chkFiles(uploadFiles, fileExtension, fileSizeLimit);
                                    
            if (FileErrMsg == null)
            {
                //上傳檔案
                List<Utils.Models.FilesModels> filesModels = new List<Utils.Models.FilesModels>();
                filesModels = FileTool.uploadFiles(uploadFiles, Server.MapPath(filePath), filePath.Replace("~/", ""));                
            }            
            return View();
        }
       
    }
}