﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 訂閱計畫管理
    /// </summary>
    [SiteAuthorize()]
    public class ProgramController : Controller
    {
        /// <summary>
        /// 訂閱計畫清單
        /// </summary>
        /// <returns></returns>
        public ActionResult ProgramList()
        {
            return View();
        }

        /// <summary>
        /// 訂閱計畫管理 - 新增
        /// </summary>
        /// <returns></returns>
        public ActionResult AddProgram()
        {
            return View();
        }

        /// <summary>
        /// 訂閱計畫管理 - 編輯
        /// </summary>
        /// <returns></returns>
        public ActionResult EditProgram()
        {
            return View();
        }
	}
}