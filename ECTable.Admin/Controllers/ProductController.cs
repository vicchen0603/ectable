﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECTable.Admin.ViewModels;
using ECTable.Admin.Models;
using System.ComponentModel;
using ECTable.DataSource;
using MySql.Data.MySqlClient;
using ECTable.Utils;
using System.Data;
using PagedList;
using Filters;
using ECTable.Admin.Libary;
using ECTable.Utils.UploadFiles;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 商品管理
    /// </summary>
    [SiteAuthorize()]
    public class ProductController : Controller
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        
        #region 商品類別管理
        /// <summary>
        /// 商品類別管理 - 列表
        /// </summary>
        /// <param name="viewModel">ProductClassViewModel</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        public ActionResult ProductClassList(ProductClassViewModel viewModel, [DefaultValue(1)]int pageNo,
            ProductClassSortingField sortingField = ProductClassSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                // 取出資料
                MySqlParameter[] para = new MySqlParameter[0];
                string sql = "select c.id as Id, c.title as Title, " + Environment.NewLine;
                sql += "ifnull((select count(id) as Total from products as p where p.class_id=c.id and p.status<>2 group by p.class_id),0) as Quantity, " + Environment.NewLine;
                sql += "c.status as Status, c.sort as Sort " + Environment.NewLine;
                sql += "from products_class as c " + Environment.NewLine;
                sql += "where c.status<>2 order by c.updatetime desc, c.createtime desc" + Environment.NewLine;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IQueryable<ProductClassItem> queryList = (from a in dt.AsEnumerable()
                                                              select new ProductClassItem()
                                                              {
                                                                  Id = Convert.ToInt32(a["Id"].ToString()),
                                                                  Title = a["Title"].ToString(),
                                                                  Quantity = Convert.ToInt32(a["Quantity"].ToString()),
                                                                  Sort = Convert.ToInt32(a["Sort"].ToString()),
                                                                  Status = Convert.ToSByte(a["Status"].ToString())
                                                              }).AsQueryable();

                    #region 排序處理
                    switch (viewModel.SortingField)
                    {
                        case ProductClassSortingField.Id:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Id);
                            else
                                queryList = queryList.OrderBy(t => t.Id);
                            break;
                        case ProductClassSortingField.Title:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Title);
                            else
                                queryList = queryList.OrderBy(t => t.Title);
                            break;
                        case ProductClassSortingField.Quantity:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Quantity);
                            else
                                queryList = queryList.OrderBy(t => t.Quantity);
                            break;
                        case ProductClassSortingField.Sort:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Sort);
                            else
                                queryList = queryList.OrderBy(t => t.Sort);
                            break;
                        case ProductClassSortingField.Status:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Status);
                            else
                                queryList = queryList.OrderBy(t => t.Status);
                            break;
                        default:
                            viewModel.SortingField = ProductClassSortingField.None;
                            viewModel.SortingDirection = SortingDirection.None;
                            break;
                    }
                    #endregion

                    // 分頁
                    // PagedList 套件參考：
                    // PagedList - ASP.NET MVC 資料分頁 MVCPaging 4.5：https://dotblogs.com.tw/joe690811/2016/07/27/040318
                    // PagedList - mrkt 的學習日記：http://kevintsengtw.blogspot.tw/2013/10/aspnet-mvc-pagedlistmvc.html
                    // PagedList - 分頁樣式 - mrkt 的學習日記：http://kevintsengtw.blogspot.tw/2013/10/aspnet-mvc-pagedlistmvc_17.html
                    viewModel.PageNo = pageNo;
                    viewModel.QueryList = queryList.ToPagedList(viewModel.PageNo, viewModel.PageSize);
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品類別管理 - 列表，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品類別管理 - 新增 - get
        /// </summary>
        /// <returns></returns>
        public ActionResult AddProductClass()
        {
            ProductClassEditViewModel viewModel = new ProductClassEditViewModel();

            try
            {
                // 查詢 Max(sort)+1
                MySqlParameter[] para = new MySqlParameter[0];
                string sql = "select max(sort)+1 from products_class where status<>2";
                string maxSort = StringTool.IIfCheckDBNull(db.ExecuteScalar(CommandType.Text, sql, para)).ToString();
                if (maxSort == "") maxSort = "1";

                try
                {
                    viewModel.Sort = int.Parse(maxSort);
                }
                catch
                {
                    viewModel.Sort = 1;
                }

                viewModel.Status = 1;
                viewModel.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品類別管理 - 新增 - get，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品類別管理 - 新增 - post
        /// </summary>
        /// <param name="viewModel">ProductClassEditViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProductClass(ProductClassEditViewModel viewModel)
        {
            try
            {
                DateTime nowTime = DateTime.Now;

                MySqlParameter[] para = new MySqlParameter[5];
                para[0] = new MySqlParameter("title", viewModel.Title);
                para[1] = new MySqlParameter("status", viewModel.Status);
                para[2] = new MySqlParameter("sort", viewModel.Sort);
                para[3] = new MySqlParameter("createid", Session["Uid"].ToString());
                para[4] = new MySqlParameter("updateid", Session["Uid"].ToString());
                string sql = "insert into products_class(" + Environment.NewLine;
                sql += "title, status, sort, createid, createtime, updateid, updatetime" + Environment.NewLine;
                sql += ") values " + Environment.NewLine;
                sql += "(" + Environment.NewLine;
                sql += "@title, @status, @sort, @createid, now(), @updateid, now()" + Environment.NewLine;
                sql += ")" + Environment.NewLine;

                if (db.ExecuteNonQuery(CommandType.Text, sql, para) == 0)
                    ViewData["Message"] = "新增失敗！";
                else
                {
                    ViewData["Message"] = "新增成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("新增失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品類別管理 - 新增 - post，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品類別管理 - 編輯 - get
        /// </summary>
        /// <param name="id">流水號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="parameter">查詢過濾資訊</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        public ActionResult EditProductClass(int? id, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize,
             ProductClassSortingField sortingField = ProductClassSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            ProductClassEditViewModel viewModel = new ProductClassEditViewModel();

            try
            {
                MySqlParameter[] para = new MySqlParameter[1];
                para[0] = new MySqlParameter("id", id);
                string sql = "select * from products_class where id=@id";
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                if (dt != null && dt.Rows.Count > 0)
                {
                    viewModel.Id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    viewModel.Title = dt.Rows[0]["title"].ToString();
                    viewModel.Status = Convert.ToSByte(dt.Rows[0]["status"].ToString());
                    viewModel.Sort = Convert.ToInt32(dt.Rows[0]["sort"].ToString());
                    viewModel.UpdateTime = Convert.ToDateTime(dt.Rows[0]["updatetime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品類別管理 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;

            return View(viewModel);
        }

        /// <summary>
        /// 商品類別管理 - 編輯 - post
        /// </summary>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="viewModel">ProductClassEditViewModel</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditProductClass([DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize, ProductClassEditViewModel viewModel,
             ProductClassSortingField sortingField = ProductClassSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                MySqlParameter[] para = new MySqlParameter[5];
                para[0] = new MySqlParameter("title", viewModel.Title);
                para[1] = new MySqlParameter("status", viewModel.Status);
                para[2] = new MySqlParameter("sort", viewModel.Sort);
                para[3] = new MySqlParameter("updateid", Session["Uid"].ToString());
                para[4] = new MySqlParameter("id", viewModel.Id);
                string sql = "update products_class set status=@status,title=@title,sort=@sort,updateid=@updateid,updatetime=now() where id=@id";
                
                int act = db.ExecuteNonQuery(CommandType.Text, sql, para);
                if (act == 0)
                {
                    ViewData["Message"] = "編輯失敗！";
                }
                else
                {
                    ViewData["Message"] = "編輯成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品類別管理 - 編輯 - post，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;

            return View(viewModel);
        }

        /// <summary>
        /// 商品類別管理 - 狀態變更
        /// </summary>
        /// <param name="id">編號</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public int EditProductClassStatus([DefaultValue(0)]int id, [DefaultValue(0)]int status)
        {
            int act = 0;

            try
            {
                MySqlParameter[] para = new MySqlParameter[3];
                string sql = "update products_class set status=@status,updateid=@updateid,updatetime=now() where id=@id";
                para[0] = new MySqlParameter("updateid", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", id);
                para[2] = new MySqlParameter("status", status);
                act = db.ExecuteNonQuery(CommandType.Text, sql, para);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("商品類別管理 - 狀態變更，發生錯誤：{0}", ex));
            }

            return act;
        }
        #endregion

        #region 烹調方式管理
        /// <summary>
        /// 烹調方式管理 - 列表
        /// </summary>
        /// <param name="viewModel">LinkMGTListViewModel</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>                
        public ActionResult ProductWayList(ProductWayViewModel viewModel, [DefaultValue(1)]int pageNo,
            [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
            ProductWaySortingField sortingField = ProductWaySortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                int n = 0;
                if (queryTitle != "")
                {
                    n += 1;
                }
                if (queryStatus != -1)
                {
                    n += 1;
                }
                //// 取出資料
                MySqlParameter[] para = new MySqlParameter[n];
                //string sql = "select c.Id, c.Title,c.Status,c.Sort " + Environment.NewLine;
                //sql += ",ifnull((select count(distinct p.id) as Total from products p inner join products_way_rel way on p.id=way.product_id where way.way_id=c.id and p.status<>2 group by p.class_id),0) as Quantity " + Environment.NewLine;
                //sql += "from products_way c where c.status<>2 " + Environment.NewLine;
                string sql = "select c.Id, c.Title,c.Status,c.Sort " + Environment.NewLine;
                sql += ",ifnull((select count(distinct p.id) as Total from products p inner join products_way_rel way on p.id=way.product_id where way.way_id=c.id and p.status<>2 group by way.way_id),0) as Quantity " + Environment.NewLine;
                sql += "from products_way c where c.status<>2 " + Environment.NewLine;
                n = 0;
                if (queryTitle != "")
                {
                    sql += " and c.Title like Concat('%' ,@Title,'%')" + Environment.NewLine;
                    para[n] = new MySqlParameter("Title", queryTitle);
                    n += 1;
                }
                if (queryStatus != -1)
                {
                    sql += " and c.Status=@Status" + Environment.NewLine;
                    para[n] = new MySqlParameter("Status", queryStatus);
                }
                sql += " order by c.updatetime desc, c.createtime desc" + Environment.NewLine;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IQueryable<ProductWayItem> queryList = (from a in dt.AsEnumerable()
                                                            select new ProductWayItem()
                                                            {
                                                                Id = Convert.ToInt32(a[0].ToString()),
                                                                Title = a[1].ToString(),
                                                                Status = Convert.ToSByte(a[2].ToString()),
                                                                Sort = Convert.ToInt32(a[3].ToString()),
                                                                Quantity = Convert.ToInt32(a[4].ToString())
                                                            }).AsQueryable();

                    #region 排序處理
                    switch (viewModel.SortingField)
                    {
                        case ProductWaySortingField.Id:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Id);
                            else
                                queryList = queryList.OrderBy(t => t.Id);
                            break;
                        case ProductWaySortingField.Title:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Title);
                            else
                                queryList = queryList.OrderBy(t => t.Title);
                            break;
                        case ProductWaySortingField.Quantity:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Quantity);
                            else
                                queryList = queryList.OrderBy(t => t.Quantity);
                            break;
                        case ProductWaySortingField.Status:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Status);
                            else
                                queryList = queryList.OrderBy(t => t.Status);
                            break;
                        case ProductWaySortingField.Sort:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Sort);
                            else
                                queryList = queryList.OrderBy(t => t.Sort);
                            break;
                        default:
                            viewModel.SortingField = ProductWaySortingField.None;
                            viewModel.SortingDirection = SortingDirection.None;
                            break;
                    }
                    #endregion

                    // 分頁
                    viewModel.PageNo = pageNo;
                    viewModel.QueryList = queryList.ToPagedList(viewModel.PageNo, viewModel.PageSize);
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("烹調方式管理 - 列表，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }
        

        /// <summary>
        /// 商品烹調方式 - 新增 - get
        /// </summary>
        /// <returns></returns>
        public ActionResult AddProductWay()
        {
            ProductWayEditViewModel viewModel = new ProductWayEditViewModel();
            
            try
            {
                viewModel.Status = 1;
                viewModel.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品烹調方式 - 新增 - get，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品烹調方式 - 新增 - post
        /// </summary>
        /// <param name="viewModel">ProductWayEditViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProductWay(ProductWayEditViewModel viewModel)
        {
            try
            {
                MySqlParameter[] para = new MySqlParameter[4];
                string sql = "insert into products_way(Status,Title,Sort,CreateID,CreateTime,UpdateID,UpdateTime)values(@Status,@Title,@Sort,@Uid,now(),@Uid,now())";
                para[0] = new MySqlParameter("UId", Session["Uid"].ToString());
                para[1] = new MySqlParameter("Title", viewModel.Title);
                para[2] = new MySqlParameter("Sort", viewModel.Sort);
                para[3] = new MySqlParameter("Status", viewModel.Status);

                int act = db.ExecuteNonQuery(CommandType.Text, sql, para);
                if (act == 0)
                {
                    ViewData["Message"] = "新增失敗！";
                }
                else
                {
                    ViewData["Message"] = "新增成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("新增失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品烹調方式 - 新增 - post，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品烹調方式 - 編輯 - get
        /// </summary>
        /// <param name="id">流水號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="parameter">查詢過濾資訊</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        public ActionResult EditProductWay(int? id, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize,
            [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
            ProductWaySortingField sortingField = ProductWaySortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            ProductWayEditViewModel viewModel = new ProductWayEditViewModel();

            try
            {
                MySqlParameter[] para = new MySqlParameter[1];
                string sql = "select * from products_way where Id=@Id";
                para[0] = new MySqlParameter("Id", id);
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                if (dt != null)
                {
                    viewModel.Id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    viewModel.Title = dt.Rows[0]["Title"].ToString();
                    viewModel.Sort = Convert.ToInt32(dt.Rows[0]["Sort"].ToString());
                    viewModel.UpdateTime = Convert.ToDateTime(dt.Rows[0]["UpdateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                    viewModel.Status = Convert.ToSByte(dt.Rows[0]["Status"].ToString());
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品烹調方式 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["QueryTitle"] = queryTitle;
            ViewData["QueryStatus"] = queryStatus;

            return View(viewModel);
        }

        /// <summary>
        /// 商品烹調方式 - 編輯 - post
        /// </summary>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="viewModel">CreateOrEditRoleViewModel</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditProductWay(ProductWayEditViewModel viewModel, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize,
            [DefaultValue("")]string queryTitle, [DefaultValue(-1)]int queryStatus,
            ProductWaySortingField sortingField = ProductWaySortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            try
            {
                MySqlParameter[] para = new MySqlParameter[5];
                string sql = "update products_way set Status=@Status,Title=@Title,Sort=@Sort,Updateid=@UId,UpdateTime=now() where id=@id";
                para[0] = new MySqlParameter("UId", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", viewModel.Id);
                para[2] = new MySqlParameter("Title", viewModel.Title);
                para[3] = new MySqlParameter("Sort", viewModel.Sort);
                para[4] = new MySqlParameter("Status", viewModel.Status);

                int act = db.ExecuteNonQuery(CommandType.Text, sql, para);
                if (act == 0)
                {
                    ViewData["Message"] = "編輯失敗！";
                }
                else
                {
                    ViewData["Message"] = "編輯成功！";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品烹調方式 - 編輯 - post，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["QueryTitle"] = queryTitle;
            ViewData["QueryStatus"] = queryStatus;

            return View(viewModel);
        }

        /// <summary>
        /// 商品烹調方式-狀態變更
        /// </summary>
        /// <param name="id">主key</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public int EditProductWayStatus([DefaultValue(0)]int id, [DefaultValue(0)]int status)
        {
            int act = 0;

            try
            {
                MySqlParameter[] para = new MySqlParameter[3];
                string sql = "update products_way set Status=@Status,Updateid=@UId,UpdateTime=now() where id=@id";
                para[0] = new MySqlParameter("Uid", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", id);
                para[2] = new MySqlParameter("Status", status);
                act = db.ExecuteNonQuery(CommandType.Text, sql, para);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("狀態變更失敗，發生錯誤：{0}", ex.Message));
            }
            
            return act;
        }
        #endregion        

        #region 商品清單
        /// <summary>
        /// 商品清單 - 列表
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="searchKeyword">搜尋關鍵字</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        public ActionResult ProductList(ProductViewModel viewModel, [DefaultValue(1)]int pageNo, string searchKeyword,
            ProductSortingField sortingField = ProductSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            // viewModel.FunctionMode - 功能模式說明：
            // NewProduct - 新增/異動商品清單：轉入後編輯 -> new
            // Product - 商品清單：erp 轉入 (僅提供檢視) -> erp

            try
            {
                viewModel.SearchKeyword = searchKeyword;

                // 取出資料
                MySqlParameter[] para = new MySqlParameter[0];
                string sql = "select id as Id, productid as ProductId, productname as ProductName, updatetime as UpdateTime, stock as Stock, " + Environment.NewLine;
                sql += "specification as Specification, price as Price, status as Status " + Environment.NewLine;
                sql += "from products where status<>2 " + Environment.NewLine;

                // 商品顯示模式判斷
                if (viewModel.FunctionMode == "Product") // 商品清單
                {
                    sql += "and updateid<>'Erp'";
                }
                else // 新增/異動商品清單
                {
                    sql += "and (updateid='Erp' or erpModifyFlag=1)";
                }

                sql += "order by updatetime desc, createtime desc" + Environment.NewLine;
                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IQueryable<ProductItem> queryList = (from a in dt.AsEnumerable()
                                                         select new ProductItem()
                                                         {
                                                             Id = Convert.ToInt32(a["Id"].ToString()),
                                                             ProductId = a["ProductId"].ToString(),
                                                             ProductName = a["ProductName"].ToString(),
                                                             UpdateTime = DateTime.Parse(a["UpdateTime"].ToString()).ToString("yyyy-MM-dd"),
                                                             Stock = Convert.ToInt32(a["Stock"].ToString()),
                                                             Specification = a["Specification"].ToString(),
                                                             Price = Convert.ToInt32(Convert.ToDecimal(a["Price"].ToString())),
                                                             Status = Convert.ToSByte(a["Status"].ToString())
                                                         }).AsQueryable();

                    #region 查詢過濾資訊
                    if (searchKeyword != null)
                    {
                        if (!string.IsNullOrEmpty(searchKeyword))
                        {
                            queryList = queryList.Where(x => x.ProductId.Contains(searchKeyword) || x.ProductName.Contains(searchKeyword));
                        }
                    }
                    #endregion

                    #region 排序處理
                    switch (viewModel.SortingField)
                    {
                        case ProductSortingField.ProductId:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.ProductId);
                            else
                                queryList = queryList.OrderBy(t => t.ProductId);
                            break;
                        case ProductSortingField.ProductName:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.ProductName);
                            else
                                queryList = queryList.OrderBy(t => t.ProductName);
                            break;
                        case ProductSortingField.UpdateTime:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.UpdateTime);
                            else
                                queryList = queryList.OrderBy(t => t.UpdateTime);
                            break;
                        case ProductSortingField.Stock:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Stock);
                            else
                                queryList = queryList.OrderBy(t => t.Stock);
                            break;
                        case ProductSortingField.Specification:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Specification);
                            else
                                queryList = queryList.OrderBy(t => t.Specification);
                            break;
                        case ProductSortingField.Price:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Price);
                            else
                                queryList = queryList.OrderBy(t => t.Price);
                            break;
                        case ProductSortingField.Status:
                            if (viewModel.SortingDirection == SortingDirection.Desc)
                                queryList = queryList.OrderByDescending(t => t.Status);
                            else
                                queryList = queryList.OrderBy(t => t.Status);
                            break;
                        default:
                            viewModel.SortingField = ProductSortingField.None;
                            viewModel.SortingDirection = SortingDirection.None;
                            break;
                    }
                    #endregion

                    // 分頁
                    viewModel.PageNo = pageNo;
                    viewModel.QueryList = queryList.ToPagedList(viewModel.PageNo, viewModel.PageSize);
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品清單 - 列表，發生錯誤：{0}", ex));
            }

            return View(viewModel);
        }

        /// <summary>
        /// 商品清單 - 編輯 - get
        /// </summary>
        /// <param name="id">流水號</param>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="functionMode">功能模式</param>
        /// <param name="searchKeyword">搜尋關鍵字</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult EditProduct(int? id, [DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize, string searchKeyword, [DefaultValue("NewProduct")]string functionMode,
            ProductSortingField sortingField = ProductSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            // viewModel.FunctionMode - 功能模式說明：
            // NewProduct - 新增/異動商品清單：轉入後編輯 -> new
            // Product - 商品清單：erp 轉入 (僅提供檢視) -> erp

            ProductEditViewModel viewModel = new ProductEditViewModel();

            viewModel.FunctionMode = functionMode;
            viewModel.UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            viewModel.FileSizeLimit = fileSizeLimit.ToString();

            // 取得下拉選單資訊
            Dictionary<int, string> productClasses = SelectListUtils.GetProductClass();
            Dictionary<int, string> productBrands = SelectListUtils.GetProductBrand();
            ViewData["ProductClasses"] = productClasses;
            ViewData["ProductBrands"] = productBrands;

            try
            {
                MySqlParameter[] para = new MySqlParameter[2];
                para[0] = new MySqlParameter("id", id);

                string sql = "select * from products where id=@id and status<>2";

                DataTable dt = db.FillDataTable(CommandType.Text, sql, para);
                if (dt != null && dt.Rows.Count > 0)
                {
                    para[1] = new MySqlParameter("productid", dt.Rows[0]["productid"].ToString());

                    viewModel.Id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    viewModel.ProductId = dt.Rows[0]["productid"].ToString();
                    viewModel.ErpProductName = dt.Rows[0]["erpproductname"].ToString();
                    viewModel.ProductName = dt.Rows[0]["productname"].ToString();
                    viewModel.Status = Convert.ToSByte(dt.Rows[0]["status"].ToString());
                    viewModel.Price = Convert.ToInt32(Convert.ToDecimal(dt.Rows[0]["price"].ToString()));
                    viewModel.BrandId = Convert.ToInt32(dt.Rows[0]["brand_id"].ToString());
                    viewModel.ClassId = Convert.ToInt32(dt.Rows[0]["class_id"].ToString());
                    viewModel.Specification = dt.Rows[0]["specification"].ToString();
                    viewModel.SeoKeyword = dt.Rows[0]["seoKeyword"].ToString();
                    viewModel.SeoDesc = dt.Rows[0]["seoDesc"].ToString();
                    viewModel.Features1 = dt.Rows[0]["features1"].ToString();
                    viewModel.Features2 = dt.Rows[0]["features2"].ToString();
                    viewModel.Features3 = dt.Rows[0]["features3"].ToString();
                    viewModel.Features4 = dt.Rows[0]["features4"].ToString();
                    viewModel.Features5 = dt.Rows[0]["features5"].ToString();
                    viewModel.Content = dt.Rows[0]["content"].ToString();
                    viewModel.UpdateId = dt.Rows[0]["updateid"].ToString();

                    // 烹調方式清單
                    // 1.列出 products_way 所有值
                    // 2.由 products_way_rel 指定 productid，比對勾選 products_way 的資訊
                    try
                    {
                        string sqlWay = "select way_id from products_way_rel where product_id=@id";
                        DataTable dtWay = db.FillDataTable(CommandType.Text, sqlWay, para);
                        if (dtWay != null && dtWay.Rows.Count > 0)
                        {
                            foreach (DataRow drWay in dtWay.Rows)
                            {
                                viewModel.WayId += drWay["way_id"].ToString() + ",";
                            }
                            if (viewModel.WayId != null) viewModel.WayId = viewModel.WayId.Substring(0, viewModel.WayId.Length - 1); // 拿掉最後的","
                        }

                        viewModel.ProductWayList = CheckListUtils.GetProductWays(viewModel.WayId);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(String.Format("商品清單-編輯，取出烹調方式清單時，發生錯誤：{0}", ex));
                    }

                    // 商品圖片
                    try
                    {
                        string sqlImage = "select * from products_images where product_id=@id";
                        DataTable dtImage = db.FillDataTable(CommandType.Text, sqlImage, para);
                        if (dtImage != null && dtImage.Rows.Count > 0)
                        {
                            int i = 1;
                            foreach (DataRow drImage in dtImage.Rows)
                            {
                                switch (i)
                                {
                                    case 1:
                                        viewModel.ExistFile1 = drImage["img_path"].ToString() + drImage["img_name"].ToString();
                                        break;
                                    case 2:
                                        viewModel.ExistFile2 = drImage["img_path"].ToString() + drImage["img_name"].ToString();
                                        break;
                                    case 3:
                                        viewModel.ExistFile3 = drImage["img_path"].ToString() + drImage["img_name"].ToString();
                                        break;
                                    case 4:
                                        viewModel.ExistFile4 = drImage["img_path"].ToString() + drImage["img_name"].ToString();
                                        break;
                                    case 5:
                                        viewModel.ExistFile5 = drImage["img_path"].ToString() + drImage["img_name"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                                i++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(String.Format("商品清單-編輯，取出商品圖片時，發生錯誤：{0}", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品清單 - 編輯 - get，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["SearchKeyword"] = searchKeyword;

            return View(viewModel);
        }

        /// <summary>
        /// 商品清單 - 編輯 - post
        /// </summary>
        /// <param name="pageNo">目前頁數</param>
        /// <param name="pageSize">分頁大小</param>
        /// <param name="searchKeyword">搜尋關鍵字</param>
        /// <param name="viewModel">ProductEditViewModel</param>
        /// <param name="sortingField">排序欄位</param>
        /// <param name="sortingDirection">排序方式</param>
        /// <returns></returns>
        /// 參考：http://kevintsengtw.blogspot.tw/2013/03/aspnet-mvc.html#.VFOrkfmUeSo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditProduct([DefaultValue(1)]int pageNo, [DefaultValue(10)]int pageSize, string searchKeyword, ProductEditViewModel viewModel,
            ProductSortingField sortingField = ProductSortingField.None, SortingDirection sortingDirection = SortingDirection.None)
        {
            string errMessage = null;
            string errUploadMessage = null;

            viewModel.UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int fileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"]);
            viewModel.FileSizeLimit = fileSizeLimit.ToString();


            try
            {
                // 更新資料邏輯說明：
                // 1.更新 products 資料
                // 2.更新 products_way_rel 資料，先刪除原資料，再新增
                // 2-1.products_way_rel，刪除資料
                // 2-2.products_way_rel，新增資料
                // 3.更新 products_images 資料，先刪除原資料，再新增
                // 3-1.products_images，刪除資料
                // 3-2.刪除原上傳檔
                // 3-3.products_images，新增資料
                // 上傳檔案
                // 附註：更新、刪除、新增的語法無法合併一次執行，需各別分開執行
                
                int pSeq = 17; // MySqlParameter 數量
                MySqlParameter[] para = new MySqlParameter[60]; // 考量動態參數，設定上限60 (wayId 除外，基本上最多會用到 32 = 17 + 3*5[檔案上傳])
                para[0] = new MySqlParameter("id", viewModel.Id);
                para[1] = new MySqlParameter("productid", viewModel.ProductId);
                para[2] = new MySqlParameter("productname", viewModel.ProductName);
                para[3] = new MySqlParameter("status", viewModel.Status);
                para[4] = new MySqlParameter("price", viewModel.Price);
                para[5] = new MySqlParameter("brandId", viewModel.BrandId);
                para[6] = new MySqlParameter("classId", viewModel.ClassId);
                para[7] = new MySqlParameter("specification", viewModel.Specification == null ? "" : viewModel.Specification);
                para[8] = new MySqlParameter("seoKeyword", viewModel.SeoKeyword == null ? "" : viewModel.SeoKeyword);
                para[9] = new MySqlParameter("seoDesc", viewModel.SeoDesc == null ? "" : viewModel.SeoDesc);
                para[10] = new MySqlParameter("features1", viewModel.Features1 == null ? "" : viewModel.Features1);
                para[11] = new MySqlParameter("features2", viewModel.Features2 == null ? "" : viewModel.Features2);
                para[12] = new MySqlParameter("features3", viewModel.Features3 == null ? "" : viewModel.Features3);
                para[13] = new MySqlParameter("features4", viewModel.Features4 == null ? "" : viewModel.Features4);
                para[14] = new MySqlParameter("features5", viewModel.Features5 == null ? "" : viewModel.Features5);
                para[15] = new MySqlParameter("content", viewModel.Content == null ? "" : viewModel.Content);
                para[16] = new MySqlParameter("updateid", Session["Uid"].ToString());

                string sqlUpadteProducts = null;
                string sqlDeleteProductsWayRel = null;
                string sqlInsertProductsWayRel = null;
                string sqlDeleteProductsImages = null;
                string sqlInsertProductsImages = null;

                // 更新資料，判斷是否執行 sql 成功，預設為 false
                bool isUpdateProducts = false;
                bool isDeleteProductsWayRel = false;
                bool isInsertProductsWayRel = false;

                // 檔案上傳，判斷是否執行成功，預設為 true
                bool isDeleteProductsImages = true;
                bool isInsertProductsImages = true;
                bool isFileUpload = true;
                bool isFileUploadCheck = true;

                // 更新 products 資料
                // 只要使用者編輯過後，一律將 erpModifyFlag 壓為 0，代表資料被改過，請不要出現在新增/異動上
                sqlUpadteProducts += "update products set productname=@productname,status=@status,price=@price,brand_id=@brandId,class_id=@classId," + Environment.NewLine;
                sqlUpadteProducts += "specification=@specification,seokeyword=@seoKeyword,seodesc=@seoDesc," + Environment.NewLine;
                sqlUpadteProducts += "features1=@features1,features2=@features2,features3=@features3,features4=@features4,features5=@features5,content=@content," + Environment.NewLine;
                sqlUpadteProducts += "updateid=@updateid,updatetime=now(),erpModifyFlag=0 where id=@id;" + Environment.NewLine;
                int actUpadteProducts = db.ExecuteNonQuery(CommandType.Text, sqlUpadteProducts, para);
                if (actUpadteProducts != 0) isUpdateProducts = true;
                else
                {
                    _logger.Error(String.Format("商品清單-編輯，更新 products 資料時，發生錯誤，para：{0}，sqlUpadteProducts：{1}", para.ToString(), sqlUpadteProducts));
                }

                if (!isUpdateProducts)
                {
                    isDeleteProductsWayRel = true;
                    isInsertProductsWayRel = true;
                }
                else
                {
                    #region 更新 products_way_rel 資料
                    // 刪除前，確認有資料需清除
                    string sqlQuery = "select * from products_way_rel where product_id=@id";
                    DataTable dt = db.FillDataTable(CommandType.Text, sqlQuery, para);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // products_way_rel，刪除資料
                        sqlDeleteProductsWayRel = "delete from products_way_rel where product_id=@id;";
                        int actDeleteProductsWayRel = db.ExecuteNonQuery(CommandType.Text, sqlDeleteProductsWayRel, para);
                        if (actDeleteProductsWayRel != 0) isDeleteProductsWayRel = true;
                        else
                        {
                            _logger.Error(String.Format("商品清單-編輯，刪除 products_way_rel 資料時，發生錯誤，para：{0}，sqlDeleteProductsWayRel：{1}", para.ToString(), sqlDeleteProductsWayRel));
                        }
                    }
                    else
                    {
                        isDeleteProductsWayRel = true;
                    }

                    // products_way_rel，新增資料，多值處理
                    string[] arrWayList = viewModel.WayId.Split(',');
                    for (int i = 0; i < arrWayList.Length; i++)
                    {
                        if (i > 0) pSeq += 1;
                        para[pSeq] = new MySqlParameter("wayId" + (i + 1), arrWayList[i].ToString());
                        sqlInsertProductsWayRel = "insert into products_way_rel (product_id, way_id) values (@id, @wayId" + (i + 1) + ");";
                        int actInsertProductsWayRel = db.ExecuteNonQuery(CommandType.Text, sqlInsertProductsWayRel, para);
                        if (actInsertProductsWayRel != 0) isInsertProductsWayRel = true;
                        else
                        {
                            isInsertProductsWayRel = false;
                            _logger.Error(String.Format("商品清單-編輯，新增 products_way_rel 資料時，發生錯誤，para：{0}，sqlInsertProductsWayRel：{1}", para.ToString(), sqlInsertProductsWayRel));
                        }
                    }
                    #endregion
                }

                if (isUpdateProducts && isDeleteProductsWayRel && isInsertProductsWayRel)
                {
                    #region 上傳檔案
                    // products_images，資料更新
                    string filePath = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFilePath"] + "/Product/";
                    string fileExtension = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileExtension"];
                    string img_path = "";
                    string img_name = "";
                    int fileSeqCount = 1; // 判斷是第幾個上傳的檔案，如：ExistFile1、ExistFile2...ExistFile5
                    int iCount = 1; // 計算有上傳幾個檔案 (pSeq 累加用)

                    foreach (var file in viewModel.UploadFiles) // 若表單有上傳檔案
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string fileErrMsg = FileTool.chkOneFiles(file, fileExtension, fileSizeLimit); // 檔案判斷
                            if (fileErrMsg != null)
                            {
                                isFileUploadCheck = false;
                                errUploadMessage += fileErrMsg;
                                _logger.Error(string.Format("商品清單-編輯，products_images.product_id：{0}，@img_name：{1}，上傳檔案判斷時，發生錯誤，{2}", viewModel.Id, img_name, errUploadMessage));
                            }
                            else // 上傳檔案檢核無誤
                            {
                                try
                                {
                                    // 上傳檔案
                                    Utils.Models.FilesModels filesModels = new Utils.Models.FilesModels();
                                    filesModels = FileTool.uploadOneFile(file, Server.MapPath(filePath), filePath);
                                    img_name = filesModels.FileName;
                                    img_path = filesModels.FilePath;

                                    // products_images，新增資料，多值處理
                                    if (fileSeqCount == 1) pSeq += fileSeqCount;

                                    para[pSeq] = new MySqlParameter("img_path" + fileSeqCount, img_path);
                                    para[pSeq + 1] = new MySqlParameter("img_name" + fileSeqCount, img_name);
                                    para[pSeq + 2] = new MySqlParameter("sort" + fileSeqCount, "0"); // toDo：沒有使用者介面，先寫死值
                                    pSeq += 3;

                                    #region products_images，刪除資料
                                    // 找對應的 ExistFile，用此值反查，再刪除資料

                                    string existFileName = null; // only file name
                                    string existFullFileName = null; // path + name

                                    switch (fileSeqCount)
                                    {
                                        case 1:
                                            existFullFileName = viewModel.ExistFile1;
                                            if (existFullFileName != null && existFullFileName.IndexOf("/") != -1) existFileName = existFullFileName.Split('/')[existFullFileName.Split('/').Length - 1];
                                            para[pSeq] = new MySqlParameter("existFile1", existFileName);
                                            sqlDeleteProductsImages = "delete from products_images where product_id=@Id and img_name=@existFile1;";
                                            pSeq++;
                                            break;
                                        case 2:
                                            existFullFileName = viewModel.ExistFile2;
                                            if (existFullFileName != null && existFullFileName.IndexOf("/") != -1) existFileName = existFullFileName.Split('/')[existFullFileName.Split('/').Length - 1];
                                            para[pSeq] = new MySqlParameter("existFile2", existFileName);
                                            sqlDeleteProductsImages = "delete from products_images where product_id=@Id and img_name=@existFile2;";
                                            pSeq++;
                                            break;
                                        case 3:
                                            existFullFileName = viewModel.ExistFile3;
                                            if (existFullFileName != null && existFullFileName.IndexOf("/") != -1) existFileName = existFullFileName.Split('/')[existFullFileName.Split('/').Length - 1];
                                            para[pSeq] = new MySqlParameter("existFile3", existFileName);
                                            sqlDeleteProductsImages = "delete from products_images where product_id=@Id and img_name=@existFile3;";
                                            pSeq++;
                                            break;
                                        case 4:
                                            existFullFileName = viewModel.ExistFile4;
                                            if (existFullFileName != null && existFullFileName.IndexOf("/") != -1) existFileName = existFullFileName.Split('/')[existFullFileName.Split('/').Length - 1];
                                            para[pSeq] = new MySqlParameter("existFile4", existFileName);
                                            sqlDeleteProductsImages = "delete from products_images where product_id=@Id and img_name=@existFile4;";
                                            pSeq++;
                                            break;
                                        case 5:
                                            existFullFileName = viewModel.ExistFile5;
                                            if (existFullFileName != null && existFullFileName.IndexOf("/") != -1) existFileName = existFullFileName.Split('/')[existFullFileName.Split('/').Length - 1];
                                            para[pSeq] = new MySqlParameter("existFile5", existFileName);
                                            sqlDeleteProductsImages = "delete from products_images where product_id=@Id and img_name=@existFile5;";
                                            pSeq++;
                                            break;
                                        default:
                                            break;
                                    }

                                    // products_images，刪除資料 
                                    try
                                    {
                                        if (existFullFileName != null)
                                        {
                                            int actDeleteProductsImages = db.ExecuteNonQuery(CommandType.Text, sqlDeleteProductsImages, para);
                                            if (actDeleteProductsImages == 0)
                                            {
                                                isDeleteProductsImages = false;
                                                _logger.Error(String.Format("商品清單-編輯，products_images.product_id：{0}，sqlDeleteProductsImages：{1}，existFullFileName：{2}，刪除 products_images 資料時，刪除失敗", viewModel.Id, sqlDeleteProductsImages, existFullFileName));
                                            }
                                            else
                                            {
                                                // 刪除實體檔案
                                                string deletePath = Server.MapPath(existFullFileName);
                                                if (System.IO.File.Exists(deletePath)) System.IO.File.Delete(deletePath);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        isDeleteProductsImages = false;
                                        _logger.Error(String.Format("商品清單-編輯，products_images.product_id：{0}，sqlDeleteProductsImages：{1}，existFullFileName：{2}，刪除 products_images 資料時，發生 Exception 錯誤：{3}", viewModel.Id, sqlDeleteProductsImages, existFullFileName, ex));
                                    }
                                    #endregion

                                    #region products_images，新增資料
                                    // 此 sql 的中間，不可加上「Environment.NewLine」，因 Substring 在算長度時，拿掉最後1碼的字會失效
                                    //sqlInsertProductsImages += "insert into products_images (product_id, img_path, img_name, sort, createid, createtime, updateid, updatetime) " + Environment.NewLine;
                                    //sqlInsertProductsImages += "values (@id, @img_path" + iCount + ", @img_name" + iCount + ", @sort, @updateid, now(), @updateid, now());";
                                    sqlInsertProductsImages = "insert into products_images (product_id, img_path, img_name, sort, createid, createtime, updateid, updatetime) ";
                                    sqlInsertProductsImages += "values (@id, @img_path" + fileSeqCount + ", @img_name" + fileSeqCount + ", @sort" + fileSeqCount + ", @updateid, now(), @updateid, now());";

                                    // products_images，新增資料
                                    try
                                    {
                                        int actInsertProductsImages = db.ExecuteNonQuery(CommandType.Text, sqlInsertProductsImages, para);
                                        if (actInsertProductsImages == 0)
                                        {
                                            isInsertProductsImages = false;
                                            _logger.Error(String.Format("商品清單-編輯，products_images.product_id：{0}，sqlInsertProductsImages：{1}，img_path：{2}，img_name：{3}，新增 products_images 資料時，新增失敗", viewModel.Id, sqlInsertProductsImages, img_path, img_name));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        isInsertProductsImages = false;
                                        _logger.Error(String.Format("商品清單-編輯，products_images.product_id：{0}，sqlInsertProductsImages：{1}，img_path：{2}，img_name：{3}，新增 products_images 資料時，發生 Exception 錯誤：{4}", viewModel.Id, sqlInsertProductsImages, img_path, img_name, ex));
                                    }
                                    #endregion

                                }
                                catch (Exception ex)
                                {
                                    isFileUpload = false;
                                    _logger.Error(string.Format("商品清單-編輯，products_images.product_id：{0}，@img_name：{1}，上傳檔案時，發生錯誤，{2}", viewModel.Id, img_name, ex));
                                }
                            }

                            iCount++;
                        }

                        fileSeqCount++;
                    }
                    #endregion
                }

                if (isUpdateProducts && isDeleteProductsWayRel && isInsertProductsWayRel && isDeleteProductsImages && isInsertProductsImages && isFileUploadCheck && isFileUpload)
                {
                    ViewData["Message"] = "編輯成功！";
                }
                else
                {
                    errMessage = "編輯失敗！\n";
                    if (!isUpdateProducts) errMessage += "更新商品失敗！\n";
                    if (!isDeleteProductsWayRel) errMessage += "刪除烹調方式失敗！\n";
                    if (!isInsertProductsWayRel) errMessage += "新增烹調方式失敗！\n";
                    if (!isDeleteProductsImages) errMessage += "刪除商品圖片失敗！\n";
                    if (!isInsertProductsImages) errMessage += "新增商品圖片失敗！\n";
                    if (!isFileUploadCheck) errMessage += "上傳檔案檢核失敗！\n";
                    if (!isFileUpload) errMessage += "上傳檔案失敗！\n";
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = String.Format("編輯失敗，發生錯誤：{0}", ex.Message);
                _logger.Error(String.Format("商品清單 - 編輯 - post，發生錯誤：{0}", ex));
            }

            ViewData["PageNo"] = pageNo; // 記錄目前頁面
            ViewData["SortingField"] = sortingField;
            ViewData["SortingDirection"] = sortingDirection;
            ViewData["PageSize"] = pageSize;
            ViewData["SearchKeyword"] = searchKeyword;

            #region 發生錯誤時，取得預設資訊
            // 取得下拉選單資訊
            Dictionary<int, string> productClasses = SelectListUtils.GetProductClass();
            Dictionary<int, string> productBrands = SelectListUtils.GetProductBrand();
            ViewData["ProductClasses"] = productClasses;
            ViewData["ProductBrands"] = productBrands;

            // 烹調方式清單
            // 1.列出 products_way 所有值
            // 2.由 products_way_rel 指定 productid，比對勾選 products_way 的資訊
            try
            {
                MySqlParameter[] para2 = new MySqlParameter[1];
                para2[0] = new MySqlParameter("productid", viewModel.ProductId);
                string sqlWay = "select id from products_way_rel where productid=@productid and status<>2";
                DataTable dtWay = db.FillDataTable(CommandType.Text, sqlWay, para2);
                if (dtWay != null && dtWay.Rows.Count > 0)
                {
                    foreach (DataRow drWay in dtWay.Rows)
                    {
                        viewModel.WayId += drWay["id"].ToString() + ",";
                    }
                    if (viewModel.WayId != null) viewModel.WayId = viewModel.WayId.Substring(0, viewModel.WayId.Length - 1); // 拿掉最後的","
                }

                viewModel.ProductWayList = CheckListUtils.GetProductWays(viewModel.WayId);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("商品清單-編輯，取出烹調方式清單時，發生錯誤：{0}", ex));
            }
            #endregion

            return View(viewModel);
        }

        /// <summary>
        /// 商品清單 - 狀態變更
        /// </summary>
        /// <param name="id">編號</param>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public int EditProductStatus([DefaultValue(0)]int id, [DefaultValue(0)]int status)
        {
            int act = 0;

            try
            {
                MySqlParameter[] para = new MySqlParameter[3];
                string sql = "update products set status=@status,updateid=@updateid,updatetime=now() where id=@id";
                para[0] = new MySqlParameter("updateid", Session["Uid"].ToString());
                para[1] = new MySqlParameter("id", id);
                para[2] = new MySqlParameter("status", status);
                act = db.ExecuteNonQuery(CommandType.Text, sql, para);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("商品清單 - 狀態變更，發生錯誤：{0}", ex));
            }

            return act;
        }
        #endregion
	}
}