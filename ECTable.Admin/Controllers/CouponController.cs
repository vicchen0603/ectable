﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Controllers
{
    /// <summary>
    /// 折價券管理
    /// </summary>
    [SiteAuthorize()]
    public class CouponController : Controller
    {
        /// <summary>
        /// 折價券清單
        /// </summary>
        /// <returns></returns>
        public ActionResult CouponList()
        {
            return View();
        }
	}
}