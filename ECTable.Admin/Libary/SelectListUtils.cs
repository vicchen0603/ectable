﻿using ECTable.DataSource;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECTable.Admin.Libary
{
    /// <summary>
    /// 提供下拉選單類
    /// </summary>
    public class SelectListUtils
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告

        /// <summary>
        /// 商品品牌 - 下拉選單
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetProductBrand()
        {
            Dictionary<int, string> productBrands = new Dictionary<int, string>();

            productBrands.Add(0, "請選擇"); // 查全部

            try
            {
                MySqlParameter[] para = new MySqlParameter[0];
                string sqlWay = "select id, title from products_brand where status<>2";
                DataTable dtWay = db.FillDataTable(CommandType.Text, sqlWay, para);
                foreach (DataRow drWay in dtWay.Rows)
                {
                    productBrands.Add(Convert.ToInt32(drWay["id"].ToString()), drWay["title"].ToString());
                }
            }
            catch
            {
                productBrands = new Dictionary<int, string>();
            }
            return productBrands;
        }

        /// <summary>
        /// 商品類別 - 下拉選單
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetProductClass()
        {
            Dictionary<int, string> productClasses = new Dictionary<int, string>();

            productClasses.Add(0, "請選擇"); // 查全部

            try
            {
                MySqlParameter[] para = new MySqlParameter[0];
                string sqlClass = "select id, title from products_class where status<>2";
                DataTable dtClass = db.FillDataTable(CommandType.Text, sqlClass, para);
                if (dtClass != null)
                {
                    foreach (DataRow drClass in dtClass.Rows)
                    {
                        productClasses.Add(Convert.ToInt32(drClass["id"].ToString()), drClass["title"].ToString());
                    }
                }
            }
            catch
            {
                productClasses = new Dictionary<int, string>();
            }
            return productClasses;
        }
    }
}