﻿using ECTable.Admin.ViewModels;
using ECTable.DataSource;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ECTable.Admin.Libary
{
    /// <summary>
    /// 提供核取方塊類
    /// </summary>
    public class CheckListUtils
    {
        private static DBUtility db = new DBUtility(DBUtility.DBServer.DB); // 資料庫連線
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告

        /// <summary>
        /// 烹調方式 - checkbox 清單
        /// </summary>
        /// <param name="checkedList">勾選清單(用","隔開)</param>
        /// <returns></returns>
        public static List<ProductWayCheckboxItem> GetProductWays(string checkedList)
        {
            List<ProductWayCheckboxItem> productWays = new List<ProductWayCheckboxItem>();

            try
            {
                string[] arrCheckList = null;
                if (checkedList != null)
                {
                    if (checkedList.IndexOf(",") != -1) arrCheckList = checkedList.Split(',');
                    else arrCheckList = new string[] { checkedList };
                }

                MySqlParameter[] para = new MySqlParameter[0];
                string sqlWay = "select id, title from products_way where status<>2";
                DataTable dtWay = db.FillDataTable(CommandType.Text, sqlWay, para);

                if (checkedList == null) // 沒有勾選
                {
                    foreach (DataRow drWay in dtWay.Rows)
                    {
                        productWays.Add(new ProductWayCheckboxItem()
                        {
                            Id = Convert.ToInt32(drWay["id"].ToString()),
                            Title = drWay["title"].ToString(),
                            Checked = false
                        });
                    }
                }
                else // 有勾選
                {
                    foreach (DataRow drWay in dtWay.Rows)
                    {
                        bool isChekcked = false;
                        if (arrCheckList.Contains(drWay["id"].ToString())) isChekcked = true;

                        productWays.Add(new ProductWayCheckboxItem()
                        {
                            Id = Convert.ToInt32(drWay["id"].ToString()),
                            Title = drWay["title"].ToString(),
                            Checked = isChekcked
                        });
                    }
                }
            }
            catch
            {
                productWays = new List<ProductWayCheckboxItem>();
            }
            return productWays;
        }
    }
}