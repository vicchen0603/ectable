﻿using NLog;
using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ECTable.Admin.Library
{
    public class SiteLibrary
    {
        protected static Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// PagedList 分頁設定
        /// </summary>
        /// <returns></returns>
        public static PagedListRenderOptions GetDefaultPagerOptions()
        {
            // 分頁樣式設定：http://kevintsengtw.blogspot.tw/2013/10/aspnet-mvc-pagedlistmvc_17.html

            return new PagedListRenderOptions()
            {
                LinkToFirstPageFormat = "«",
                LinkToPreviousPageFormat = "上一頁",
                LinkToNextPageFormat = "下一頁",
                LinkToLastPageFormat = "»",
                DisplayLinkToFirstPage = PagedListDisplayMode.Always,
                DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
                DisplayLinkToNextPage = PagedListDisplayMode.Always,
                DisplayLinkToLastPage = PagedListDisplayMode.Always
                //PageCountAndCurrentLocationFormat = "第{0}頁/共{1}頁",
                //DisplayPageCountAndCurrentLocation = true
            };
        }

        /// <summary>
        /// 取得客戶端真實IP，如果有代理則第一個非內網地址
        /// </summary>
        /// 參考網址：http://tc.wangchao.net.cn/bbs/detail_544486.html
        public static string GetIPAddress
        {
            get
            {
                string result = String.Empty;
                result = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (result != null && result != String.Empty)
                {
                    // 可能有代理
                    if (result.IndexOf(".") == -1)    // 沒有"."肯定是非IPv4格式
                        result = null;
                    else
                    {
                        if (result.IndexOf(",") != -1)
                        {
                            // 有","，估計多個代理，取第一個不是內網的IP
                            result = result.Replace(" ", "").Replace("'", "");
                            string[] temparyip = result.Split(",;".ToCharArray());
                            for (int i = 0; i < temparyip.Length; i++)
                            {
                                if (IsIPAddress(temparyip[i])
                                    && temparyip[i].Substring(0, 3) != "10."
                                    && temparyip[i].Substring(0, 7) != "192.168"
                                    && temparyip[i].Substring(0, 7) != "172.16.")
                                {
                                    return temparyip[i];    // 找到不是內網的地址
                                }
                            }
                        }
                        else if (IsIPAddress(result)) // 代理即是IP格式
                            return result;
                        else
                            result = null;    // 代理中的內容 非IP，取IP
                    }
                }

                string IpAddress = (result != null && result != String.Empty) ? result : System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                if (null == result || result == String.Empty) result = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                if (result == null || result == String.Empty) result = System.Web.HttpContext.Current.Request.UserHostAddress;

                return result;
            }
        }

        /// <summary>
        /// 判斷是否是IP地址格式 0.0.0.0
        /// </summary>
        /// <param name="str1">待判斷的IP地址</param>
        /// <returns>true or false</returns>
        public static bool IsIPAddress(string str1)
        {
            if (str1 == null || str1 == string.Empty || str1.Length < 7 || str1.Length > 15) return false;
            string regformat = @"^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$";
            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);
            return regex.IsMatch(str1);
        }
    }
}