﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters
{
    public class SiteAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // 尚未登入，重導回登入畫面
            if (!IsLogin(filterContext.HttpContext))
            {
                // 記錄回傳時需導頁的retureUrl
                
                filterContext.Result = new RedirectToRouteResult(
                                       new System.Web.Routing.RouteValueDictionary
                                       {
                                           { "controller", "Login" },
                                           { "action", "Login" }
                                       });
                return;
            }
        }
        private bool IsLogin(HttpContextBase httpContext)
        {
           if(httpContext.Session["Uid"]==null)
               return false;
           else          
               return true;
        }
    }
}