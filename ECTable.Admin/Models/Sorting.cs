﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECTable.Admin.Models
{
    /// <summary>
    /// 排序方式
    /// </summary>
    public enum SortingDirection
    {
        None,
        Desc,
        Asc
    }

    /// <summary>
    /// 排序欄位 - 帳號管理 - Manager
    /// </summary>
    public enum ManagerSortingField
    {
        None,
        Uid, // 編號
        LoginIpAddress, // 最後登入IP
        LoginTime, // 最後登入日期
        Status // 狀態
    }

    #region 商品管理 - Product
    /// <summary>
    /// 排序欄位 - 商品類別管理 - ProductClass
    /// </summary>
    public enum ProductClassSortingField
    {
        None,
        Id, // 編號
        Title, // 類別名稱
        Quantity, // 商品數量
        Sort,//排序
        Status // 狀態
    }

    /// <summary>
    /// 排序欄位 - 商品烹調方式管理 - ProductWay
    /// </summary>
    public enum ProductWaySortingField
    {
        None,
        Id, // 編號
        Title, // 烹調方式
        Quantity, // 商品數量
        Sort,//排序
        Status // 狀態
    }

    /// <summary>
    /// 排序欄位 - 商品清單 - Product
    /// </summary>
    public enum ProductSortingField
    {
        None,
        ProductId, // 編號
        ProductName, // 名稱
        UpdateTime, // 最後編輯日期
        Stock, // 庫存
        Specification, // 規格
        Price, // 售價
        Status // 上架狀態
    }
    #endregion

    /// <summary>
    /// 排序欄位 - 品牌管理 - 品牌清單 - Brand
    /// </summary>
    public enum BrandSortingField
    {
        None,
        Id, // 編號
        Title, // 名稱
        UpdateTime, // 最後編輯日期
        Quantity, // 商品數
        Status, // 上架狀態
        Sort // 排序
    }

    /// <summary>
    /// 排序欄位 - 策展管理 - 策展清單 - Curation
    /// </summary>
    public enum CurationSortingField
    {
        None,
        Id, // 編號
        NonPrice, // 日期 (展覽期間)
        Title, // 策展名稱 (展覽名稱)
        Price, // 定價 (Nonprice) / 特價 (Price)
        Quantity, // 銷售套數
        Status // 上架狀態
    }

    /// <summary>
    /// 排序欄位 - 訂閱計劃管理 - 訂閱計劃清單 - Program
    /// </summary>
    public enum ProgramSortingField
    {
        None,
        Id, // 編號
        CreateTime, // 建立日期
        Title, // 訂閱計畫
        Quantity, // 有效訂閱數
        Status // 上架狀態
    }
}