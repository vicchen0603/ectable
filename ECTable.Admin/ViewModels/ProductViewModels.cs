﻿using ECTable.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace ECTable.Admin.ViewModels
{
    public class SearchItem
    {
        /// <summary>
        /// 搜尋關鍵字
        /// </summary>
        public string SearchKeyword { get; set; }
    }

    /// <summary>
    /// 烹調方式 - Item - checkbox list 用
    /// </summary>
    public class ProductWayCheckboxItem
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 烹調類別名稱
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 是否勾選 (true:是,false:否)
        /// </summary>
        public bool Checked { get; set; }
    }

    #region 商品類別管理
    /// <summary>
    /// 商品類別管理 - ViewModel
    /// </summary>
    public class ProductClassViewModel
    {
        /// <summary>
        /// 目前頁數
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 每頁大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 目前頁內容
        /// </summary>
        public IPagedList<ProductClassItem> QueryList { get; set; }

        /// <summary>
        /// 目前排序欄位
        /// </summary>
        public ProductClassSortingField SortingField { get; set; }

        /// 目前排序方向
        /// </summary>
        public SortingDirection SortingDirection { get; set; }

        public ProductClassViewModel()
        {
            this.PageNo = 1;
            this.PageSize = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"]);
            this.SortingField = ProductClassSortingField.None;
            this.SortingDirection = SortingDirection.None;
        }
    }

    /// <summary>
    /// 商品類別管理 - Item
    /// </summary>
    public class ProductClassItem
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 類別名稱
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 商品數量
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public sbyte Status { get; set; }
    }

    /// <summary>
    /// 商品類別管理 - EditViewModel
    /// </summary>
    public class ProductClassEditViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 類別名稱
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 上架設定 (0：下線、1：上線、2：刪除)
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 排序設定
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 新增時間 - 顯示用
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 編輯時間 - 顯示用
        /// </summary>
        public string UpdateTime { get; set; }
    }
    #endregion

    #region 烹調方式管理

    /// <summary>
    /// 商品烹調方式管理 - ViewModel
    /// </summary>
    public class ProductWayViewModel
    {

        /// <summary>
        /// 烹調方式
        /// </summary>
        public string QueryTitle { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int QueryStatus { get; set; }

        /// <summary>
        /// 目前頁數
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 每頁大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 目前頁內容
        /// </summary>
        public IPagedList<ProductWayItem> QueryList { get; set; }

        /// <summary>
        /// 目前排序欄位
        /// </summary>
        public ProductWaySortingField SortingField { get; set; }

        /// 目前排序方向
        /// </summary>
        public SortingDirection SortingDirection { get; set; }

        public ProductWayViewModel()
        {
            this.QueryStatus = -1;
            this.PageNo = 1;
            this.PageSize = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"]);
            this.SortingField = ProductWaySortingField.None;
            this.SortingDirection = SortingDirection.None;
        }
    }

    /// <summary>
    /// 商品烹調方式管理 - Item
    /// </summary>
    public class ProductWayItem
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 類別名稱
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public sbyte Status { get; set; }
    }

    /// <summary>
    /// 商品烹調方式管理 - EditViewModel
    /// </summary>
    public class ProductWayEditViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 類別名稱
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 上架設定
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 排序設定
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 新增時間 - 顯示用
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 編輯時間 - 顯示用
        /// </summary>
        public string UpdateTime { get; set; }
    }
    #endregion

    #region 商品清單
    /// <summary>
    /// 商品清單 - ViewModel
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// 功能模式 (Product：商單清單，NewProduct：新增/異動商品清單)
        /// </summary>
        public string FunctionMode { get; set; }

        /// <summary>
        /// 目前頁數
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 每頁大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 搜尋關鍵字
        /// </summary>
        public string SearchKeyword { get; set; }

        /// <summary>
        /// 目前頁內容
        /// </summary>
        public IPagedList<ProductItem> QueryList { get; set; }

        /// <summary>
        /// 目前排序欄位
        /// </summary>
        public ProductSortingField SortingField { get; set; }

        /// 目前排序方向
        /// </summary>
        public SortingDirection SortingDirection { get; set; }

        public ProductViewModel()
        {
            this.FunctionMode = "NewProduct";
            this.PageNo = 1;
            this.PageSize = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"]);
            this.SortingField = ProductSortingField.None;
            this.SortingDirection = SortingDirection.None;
        }
    }

    /// <summary>
    /// 商品清單 - Item
    /// </summary>
    public class ProductItem
    {
        /// <summary>
        /// 編號 (p-key，清單不顯示，找對應資料用)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 商品編號
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 最後編輯日期 (yyyy-MM-dd)
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 庫存
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// 規格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 售價
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// 上架狀態 (0：下線、1：上線、2：刪除)
        /// </summary>
        public sbyte Status { get; set; }
    }
    
    /// <summary>
    /// 商品清單 - EditViewModel
    /// </summary>
    public class ProductEditViewModel
    {
        /// <summary>
        /// 功能模式 (Product：商單清單，NewProduct：新增/異動商品清單)
        /// </summary>
        public string FunctionMode { get; set; }

        /// <summary>
        /// 編號 (p-key，清單不顯示，找對應資料用)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 商品編號
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// ERP商品名稱
        /// </summary>
        public string ErpProductName { get; set; }

        /// <summary>
        /// 編輯時間 (抓系統現在時間：yyyy-MM-dd HH:mm:ss)
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 上架設定 (0：下線、1：上線、2：刪除)
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 售價
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// 商品品牌 (單選)
        /// </summary>
        public int BrandId { get; set; }

        /// <summary>
        /// 商品類別 (單選)
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// 烹調方式 (複選)
        /// </summary>
        public string WayId { get; set; }

        /// <summary>
        /// 烹調方式清單 - 顯示用
        /// </summary>
        public List<ProductWayCheckboxItem> ProductWayList { get; set; }

        /// <summary>
        /// 商品規格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 檔案
        /// </summary>
        public IEnumerable<HttpPostedFileBase> UploadFiles { get; set; }

        /// <summary>
        /// 已存在檔案1
        /// </summary>
        public string ExistFile1 { get; set; }

        /// <summary>
        /// 已存在檔案2
        /// </summary>
        public string ExistFile2 { get; set; }

        /// <summary>
        /// 已存在檔案3
        /// </summary>
        public string ExistFile3 { get; set; }

        /// <summary>
        /// 已存在檔案4
        /// </summary>
        public string ExistFile4 { get; set; }

        /// <summary>
        /// 已存在檔案5
        /// </summary>
        public string ExistFile5 { get; set; }

        /// <summary>
        /// 檔案大小上限 - 顯示用
        /// </summary>
        public string FileSizeLimit { get; set; }

        /// <summary>
        /// 商品關鍵字
        /// </summary>
        public string SeoKeyword { get; set; }

        /// <summary>
        /// 商品簡介
        /// </summary>
        public string SeoDesc { get; set; }

        /// <summary>
        /// 商品特色 - 1
        /// </summary>
        public string Features1 { get; set; }

        /// <summary>
        /// 商品特色 - 2
        /// </summary>
        public string Features2 { get; set; }

        /// <summary>
        /// 商品特色 - 3
        /// </summary>
        public string Features3 { get; set; }

        /// <summary>
        /// 商品特色 - 4
        /// </summary>
        public string Features4 { get; set; }

        /// <summary>
        /// 商品特色 - 5 (一行一個欄位，剩下的放第5行)
        /// </summary>
        public string Features5 { get; set; }

        /// <summary>
        /// 商品描述 (含 html)
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 編輯帳號
        /// </summary>
        public string UpdateId { get; set; }

        public ProductEditViewModel()
        {
            this.FunctionMode = "NewProduct";
        }
    }
    #endregion
}