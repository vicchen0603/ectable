﻿using ECTable.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace ECTable.Admin.ViewModels
{
    public class ManagerViewModel
    {
        /// <summary>
        /// 是否能新增帳號
        /// </summary>
        public string IsAdd { get; set; }

        /// <summary>
        /// 目前頁數
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 每頁大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 目前頁內容
        /// </summary>
        public IPagedList<ManagerItem> QueryList { get; set; }

        /// <summary>
        /// 目前排序欄位
        /// </summary>
        public ManagerSortingField SortingField { get; set; }

        /// 目前排序方向
        /// </summary>
        public SortingDirection SortingDirection { get; set; }

        public ManagerViewModel()
        {
            this.PageNo = 1;
            this.PageSize = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"]);
            this.SortingField = ManagerSortingField.None;
            this.SortingDirection = SortingDirection.None;
        }
    }

    /// <summary>
    /// 帳號管理 - Item
    /// </summary>
    public class ManagerItem
    {
        /// <summary>
        /// 編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 帳號
        /// </summary>
        public string UID { get; set; }

        /// <summary>
        /// 最後登入IP
        /// </summary>
        public string Login_IpAddress { get; set; }

        /// <summary>
        /// 最後登入日期
        /// </summary>
        public string LoginTime { get; set; }

        /// <summary>
        /// 狀態0代表停用1代表正常
        /// </summary>
        public sbyte  Status { get; set; }        
    }

    /// <summary>
    /// 帳號管理 - EditViewModel
    /// </summary>
    public class ManagerEditViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 帳號
        /// </summary>
        public string UID { get; set; }
        /// <summary>
        /// 密碼
        /// </summary>
        public string Pwd { get; set; }

        /// <summary>
        /// 狀態0代表停用1代表正常
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 超級管理員0代表停用1代表正常
        /// </summary>
        public sbyte IsUserPower { get; set; }

        /// <summary>
        /// 新增時間 - 顯示用
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 編輯時間 - 顯示用
        /// </summary>
        public string UpdateTime { get; set; }
    }
}