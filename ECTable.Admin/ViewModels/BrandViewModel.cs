﻿using ECTable.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace ECTable.Admin.ViewModels
{
    /// <summary>
    /// 品牌管理 - ViewModel
    /// </summary>
    public class BrandViewModel
    {

        /// <summary>
        /// 名稱
        /// </summary>
        public string QueryTitle { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int QueryStatus { get; set; }

        /// <summary>
        /// 目前頁數
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 每頁大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 目前頁內容
        /// </summary>
        public IPagedList<BrandItem> QueryList { get; set; }

        /// <summary>
        /// 目前排序欄位
        /// </summary>
        public BrandSortingField SortingField { get; set; }

        /// 目前排序方向
        /// </summary>
        public SortingDirection SortingDirection { get; set; }

        public BrandViewModel()
        {
            this.QueryStatus = -1;
            this.PageNo = 1;
            this.PageSize = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"]);
            this.SortingField = BrandSortingField.None;
            this.SortingDirection = SortingDirection.None;
        }
    }

    /// <summary>
    /// 品牌管理 - Item
    /// </summary>
    public class BrandItem
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 類別名稱
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 最後編輯日期
        /// </summary>
        public string UpdateTime { get; set; }
    }

    /// <summary>
    /// 品牌管理- EditViewModel
    /// </summary>
    public class BrandEditViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 圖片路徑+圖片名稱
        /// </summary>
        public string ExistFile { get; set; }
        
        /// <summary>
        /// 品牌關鍵字(Seo) 
        /// </summary>
        public string SeoKeyword { get; set; }

        /// <summary>
        /// 品牌簡介(Seo) 	
        /// </summary>
        public string SeoDesc { get; set; }

        /// <summary>
        /// 品牌描述(html)  
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 排序設定
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 上架/下架
        /// </summary>
        public sbyte Status { get; set; }

        /// <summary>
        /// 新增時間 - 顯示用
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 編輯時間 - 顯示用
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 上傳檔案
        /// </summary>
        public HttpPostedFileBase uploadFiles { get; set; }

        /// <summary>
        /// 檔案大小上限 - 顯示用
        /// </summary>
        public string FileSizeLimit { get; set; }
    }
}