﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ECTable.Utils.Security
{
    public class SecurityPlus
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="value">字串</param>
        /// <returns>回傳值</returns>
        public static string encryptMd5(string value)
        {
            string result = "";
            using (MD5CryptoServiceProvider obj = new MD5CryptoServiceProvider())
            {    
                byte[] byte_result = obj.ComputeHash(Encoding.Default.GetBytes(value));
                result= BitConverter.ToString(byte_result).Replace("-", "").ToUpper();
            }
           return result;
        }
    }
}
