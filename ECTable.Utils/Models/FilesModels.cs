﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECTable.Utils.Models
{
    public class FilesModels
    {
        /// <summary>
        /// 檔案路徑
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 檔案名稱
        /// </summary>
        public string FileName { get; set; }
    }
}
