﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ECTable.Utils.Helpers
{
    public static class SortingHelper
    {
        /// <summary>
        /// 產生 Sorting Column
        /// 用法:@Html.SortingColumnCust("類別名稱", BulletinClassSortingField.Name, Model.Parameter.SortingField, Model.Parameter.SortingDirection)
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="field">要排序的欄位</param>
        /// <param name="sortingField">目前排序的欄位</param>
        /// <param name="direction">排序方向</param>
        /// <returns></returns>
        public static MvcHtmlString SortingColumnCust(this HtmlHelper htmlHelper, string columnName, Enum field, Enum sortingField, Enum direction)
        {
            var link = new TagBuilder("a");
            //增加屬性
            link.SetInnerText(columnName);
            link.MergeAttribute("href", "javascript: void(0)");
            link.MergeAttribute("onclick", string.Format("changeSortingField('{0}','{1}','{2}');", field.ToString(), sortingField.ToString(), direction.ToString()));
            var linkString = link.ToString(TagRenderMode.Normal);
            if (field.Equals(sortingField))
            {
                var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
                var tempString = "";
                var upDownTemplate = "<a class='{0}' href='#'><img src='{1}' /></a>";
                var upDownLink = new TagBuilder("a");
                var directionStr = direction.ToString().ToUpper();

                if (directionStr == "ASC")
                {
                    var upIcon = urlHelper.Content("~/img/icon_up.png");
                    tempString = string.Format(upDownTemplate, "sortUp", upIcon);
                }
                else
                {
                    var downIcon = urlHelper.Content("~/img/icon_down.png");
                    tempString = string.Format(upDownTemplate, "sortDown", downIcon);
                }
                linkString += tempString;
            }

            return MvcHtmlString.Create(linkString);
        }
    }
}
