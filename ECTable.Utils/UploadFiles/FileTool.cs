﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ECTable.Utils.UploadFiles
{
    public class FileTool
    {
        public static Encoding GetEncoding(string filePath)
        {
            Encoding result = System.Text.Encoding.Default;
            FileStream file = null;
            try
            {
                file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                if (file.CanSeek)
                {
                    //   get   the   bom,   if   there   is   one   
                    byte[] bom = new byte[4];
                    file.Read(bom, 0, 4);

                    //   utf-8   
                    if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
                        result = System.Text.Encoding.UTF8;
                    //   ucs-2le,   ucs-4le,   ucs-16le,   utf-16,   ucs-2,   ucs-4   
                    else if ((bom[0] == 0xff && bom[1] == 0xfe) ||
                    (bom[0] == 0xfe && bom[1] == 0xff) ||
                    (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff))
                        result = System.Text.Encoding.Unicode;
                    //   else   ascii   
                    else
                        result = System.Text.Encoding.Default;
                }
                else
                {
                    //   can't   detect,   set   to   default   
                    result = System.Text.Encoding.Default;
                }

                return result;
            }
            finally
            {
                if (null != file) file.Close();
            }
        }
        /// <summary>
        /// 判斷檔案
        /// </summary>
        /// <param name="UploadFiles">檔案</param>
        /// <param name="ImgExt">副檔名</param>
        /// <param name="FileSize">檔案大小</param>
        /// <returns>如果回傳的字串是null代表ok</returns>
        public static string chkFiles(IEnumerable<HttpPostedFileBase> uploadFiles, string imgExt, int fileSize)
        {
            int fileCount = 0;
            string errorMsg = null;
            string fileExt = "";
            foreach (var file in uploadFiles)
            {
                // 檢查是否有選擇檔案
                if (file != null && file.ContentLength > 0)
                {
                    //// 檢查檔案類型
                    fileExt = Path.GetExtension(file.FileName).ToLower();

                    if (imgExt.IndexOf(fileExt) == -1)
                        errorMsg += String.Format("新增失敗，檔名：{0}，上傳檔案限制的副檔名！\\n", Path.GetFileName(file.FileName));

                    // 檢查檔案大小要限制-3MB
                    if (file.ContentLength > 1024*fileSize)
                        errorMsg += String.Format("新增失敗，檔名：{0}，單一上傳檔案超過 " + fileSize + "KB 限制！\\n", Path.GetFileName(file.FileName));

                    fileCount += 1;
                }
                else
                {
                    if (file != null)
                        errorMsg += String.Format("新增失敗，檔名：{0}，上傳檔案為空的檔案！\\n", Path.GetFileName(file.FileName));
                    else
                        errorMsg += String.Format("請上傳檔案！\\n");
                }
            }
            return errorMsg;
        }
        /// <summary>
        /// 單筆判斷
        /// </summary>
        /// <param name="UploadFiles">檔案</param>
        /// <param name="ImgExt">副檔名</param>
        /// <param name="FileSize">檔案大小</param>
        /// <returns>如果回傳的字串是null代表ok</returns>
        public static string chkOneFiles(HttpPostedFileBase file, string imgExt, int fileSize)
        {
            int fileCount = 0;
            string errorMsg = null;
            string fileExt = "";
            // 檢查是否有選擇檔案
            if (file != null && file.ContentLength > 0)
            {
                //// 檢查檔案類型
                fileExt = Path.GetExtension(file.FileName).ToLower();

                if (imgExt.IndexOf(fileExt) == -1)
                    errorMsg += String.Format("新增失敗，檔名：{0}，上傳檔案限制的副檔名！\\n", Path.GetFileName(file.FileName));

                // 檢查檔案大小要限制-3MB
                if (file.ContentLength > 1024 * fileSize)
                    errorMsg += String.Format("新增失敗，檔名：{0}，單一上傳檔案超過 " + fileSize + "KB 限制！\\n", Path.GetFileName(file.FileName));

                fileCount += 1;
            }
            else
            {
                if (file != null)
                    errorMsg += String.Format("新增失敗，檔名：{0}，上傳檔案為空的檔案！\\n", Path.GetFileName(file.FileName));
                else
                    errorMsg += String.Format("請上傳檔案！\\n");
            }
            return errorMsg;
        }
        /// <summary>
        /// 多筆上傳檔案
        /// </summary>
        /// <param name="uploadFiles">檔案</param>
        /// <param name="filePath">路徑</param>
        /// <param name="filePathName">儲存的路徑</param>
        /// <returns>回傳Models.FilesModels</returns>
        public static List<Models.FilesModels > uploadFiles(IEnumerable<HttpPostedFileBase> uploadFiles,string filePath,string filePathName)
        {
            List <Models .FilesModels > filesModels=new List<Models.FilesModels>();            
            string saveFileName = "";
            //如果沒這資料夾就建這資料夾
            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);
            foreach (var file in uploadFiles)
            {
                if (file != null)
                {
                    string fileName = Path.GetFileName(file.FileName);
                    saveFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + fileName;
                    string savedName = Path.Combine(filePath, saveFileName);
                    file.SaveAs(savedName);
                    filesModels.Add(new Models.FilesModels()
                    {
                        FileName = saveFileName,
                        FilePath = filePathName
                    });
                }
            }
            return filesModels;
        }
        /// <summary>
        /// 單筆上傳
        /// </summary>
        /// <param name="file">檔案</param>
        /// <param name="filePath">路徑</param>
        /// <param name="filePathName">儲存的路徑</param>
        /// <returns></returns>
        public static Models.FilesModels uploadOneFile(HttpPostedFileBase file, string filePath, string filePathName)
        {
            Models.FilesModels filesModels = new Models.FilesModels();
            string saveFileName = "";
            //如果沒這資料夾就建這資料夾
            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);
            if (file != null)
            {
                string fileName = Path.GetFileName(file.FileName);
                saveFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + fileName;
                string savedName = Path.Combine(filePath, saveFileName);
                file.SaveAs(savedName);
                filesModels.FileName=saveFileName;
                filesModels.FilePath=filePathName;              
            }
            return filesModels;
        }
    }
}