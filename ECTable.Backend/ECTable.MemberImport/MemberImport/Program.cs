﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using NLog;
using System.IO;

namespace MemberImport
{
    class Program
    {
        static string path = ConfigurationSettings.AppSettings["path"].ToString();
        static string backPath = ConfigurationSettings.AppSettings["backPath"].ToString();
        static DBUtility db = new DBUtility(DBUtility.DBServer.DB);
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        static bool isError = false;

        static void Main(string[] args)
        {
            _logger.Info("============ Start =============");

            try
            {
                if (File.Exists(path) == false)
                {
                    _logger.Info(String.Format("Main 執行時，檔案不存在，檔案路徑：{0}", path));
                }
                else
                {
                    string backFileName = DateTime.Now.ToString("yyyyMMdd") + "-member.xls";

                    DataTable dt;
                    dt = GetExcelDataTable(); // 讀取檔案
                    string Sql;
                    int a = dt.Rows.Count;

                    foreach (DataRow row in dt.Rows)
                    {
                        try
                        {
                            MySqlParameter[] para1 = new MySqlParameter[1];
                            para1[0] = new MySqlParameter("login_account", row["login_account"].ToString());
                            Sql = "select ID from member where login_account=@login_account";
                            string ID = ComUtility.StringTool.IIfCheckDBNull(db.ExecuteScalar(CommandType.Text, Sql, para1)).ToString();
                            if (ID == "")
                            {
                                MySqlParameter[] para = new MySqlParameter[14];
                                para[0] = new MySqlParameter("fullname", row["fullname"].ToString());
                                para[1] = new MySqlParameter("login_password", row["login_password"].ToString());
                                para[2] = new MySqlParameter("gender", row["gender"].ToString());
                                para[3] = new MySqlParameter("birthday", row["birthday"].ToString());
                                para[4] = new MySqlParameter("Email", row["Email"].ToString());
                                para[5] = new MySqlParameter("login_account", row["login_account"].ToString());
                                para[6] = new MySqlParameter("celnum", row["celnum"].ToString());
                                para[7] = new MySqlParameter("telnum", row["telnum"].ToString());
                                para[8] = new MySqlParameter("telnum_ext", row["telnum_ext"].ToString());
                                para[9] = new MySqlParameter("address_city_code", row["address_city_code"].ToString());
                                para[10] = new MySqlParameter("address_county_name", row["address_county_name"].ToString());
                                para[11] = new MySqlParameter("address_city_name", row["address_city_name"].ToString());
                                para[12] = new MySqlParameter("address_detail", row["address_detail"].ToString());
                                para[13] = new MySqlParameter("info_date_for_join", row["info_date_for_join"].ToString());

                                Sql = "insert into member(fullname,login_password,gender,birthday,Email,login_account,celnum,telnum,telnum_ext,address_city_code,address_county_name,address_city_name,address_detail,info_date_for_join)" + Environment.NewLine;
                                Sql += "values" + Environment.NewLine;
                                Sql += "(@fullname,@login_password,@gender,@birthday,@Email,@login_account,@celnum,@telnum,@telnum_ext,@address_city_code,@address_county_name,@address_city_name,@address_detail,@info_date_for_join)" + Environment.NewLine;

                                db.ExecuteNonQuery(CommandType.Text, Sql, para);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(String.Format("Main 執行，login_account：{0}，讀取逐筆資料，新增時，發生錯誤：{1}", row["login_account"].ToString(), ex));
                        }
                    }

                    // 搬移檔案
                    if (!isError) File.Move(path, backPath + backFileName);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("Main 執行時，發生錯誤：{0}", ex));
            }

            _logger.Info("============ End =============");

            //Console.ReadKey(); // 保留視窗不關，debug 用
        }

        /// <summary>
        /// 讀取檔案
        /// </summary>
        /// <returns>DataTable</returns>
        static DataTable GetExcelDataTable()
        {
            //Office 2003
            OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1;Readonly=0'");

            //Office 2007
            //OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 12.0 Xml;HDR=YES'");

            DataTable dt = new DataTable();

            try
            {
                OleDbDataAdapter da = new OleDbDataAdapter("select * from [工作表1$]", conn);

                da.Fill(dt);
                dt.TableName = "tmp";
            }
            catch (Exception ex)
            {
                isError = true;
                _logger.Error(String.Format("GetExcelDataTable 執行時，path：{0}，發生錯誤：{1}", path, ex));
            }
            
            conn.Close();

            return dt;
        }
    }
}
