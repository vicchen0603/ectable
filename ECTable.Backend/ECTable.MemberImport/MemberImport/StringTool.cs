﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;
namespace ComUtility
{
   public class StringTool
    {

        /// <summary>
        /// 判斷字串是否包含英數字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool ChkStr(string Str)
        {
            bool Chk = true;
            if (!string.IsNullOrEmpty(Str))
            {
                string s = "0123456789";
                int k = 0;
                for (int i = 0; i <= Str.Length - 1; i++)
                {
                    string o = Str.Substring(i, 1);
                    if (s.IndexOf(o) >= 0)
                    {
                        k += 1;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for (int i = 0; i <= Str.Length - 1; i++)
                {
                    string o = Str.Substring(i, 1);
                    if (s.IndexOf(o) >= 0)
                    {
                        k += 1;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (k == 2)
                {
                    Chk = true;
                }
                else
                {
                    Chk = false;
                }
            }
            else
            {
                Chk = true;
            }
            return Chk;
        }

        public static string IIfCheckDBNull(string str_Value)
        {
            if (string.IsNullOrEmpty(str_Value))
            {
                return "";
            }
            else
            {
                return str_Value;
            }
        }

        /// <summary>
        /// 純數字判斷
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNumber(string str)
        {
            Regex reg1 = new Regex(@"^[0-9]\d*$");

            return reg1.IsMatch(str);

        }

        /// <summary>
        /// 判斷是否有已此字串
        /// </summary>
        /// <param name="IDs">總字串</param>
        /// <param name="ChkID">需判斷的ID</param>
        /// <param name="Str">隔開的字串</param>
        /// <returns></returns>
        public static bool ChkID(string IDs, string ChkID,string Str)
        {
            if (IIfCheckDBNull(IDs) == "")
            {
                return false;
            }
            else
            {
                bool Chk = false;
                foreach (string ID in IDs.Split(new string[] { Str }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (ID.Trim().ToLower() == ChkID.Trim().ToLower())
                    {
                        Chk = true;
                        break;
                    }
                }
                return Chk;
            }
        }

       /// <summary>
       /// 將特殊字串取代成空白ex{test}取代成""
       /// </summary>
       /// <param name="S_Str"></param>
       /// <param name="E_Str"></param>
       /// <param name="StrHtml"></param>
       /// <returns></returns>
        public static string Clear_SpecialChar(string S_Str, string E_Str, string StrHtml)
        {
            if (string.IsNullOrEmpty(StrHtml))
            {
                return "";
            }
            else
            {

                int SL = StrHtml.IndexOf(S_Str);
                int EL = StrHtml.IndexOf(E_Str);
                if (SL != -1 & EL != -1)
                {
                    if (SL < EL)
                    {
                        string ReplaceStr = StrHtml.Substring(SL, EL - SL + E_Str.Length);
                        StrHtml = StrHtml.Replace(ReplaceStr, "");
                        return Clear_SpecialChar(S_Str, E_Str, StrHtml);
                    }
                    else
                    {
                        return StrHtml.Trim();
                    }
                }
                else
                {
                    return StrHtml.Trim();
                }
            }
        }

        /// <summary>
        /// 取得特殊字串裡面的值
        /// </summary>
        /// <param name="S_Str"></param>
        /// <param name="E_Str"></param>
        /// <param name="StrHtml"></param>
        /// <returns></returns>
        public static string Get_SpecialChar(string S_Str, string E_Str, string StrHtml,string Result)
        {
            if (string.IsNullOrEmpty(StrHtml))
            {
                return Result;
            }
            else
            {

                int SL = StrHtml.IndexOf(S_Str);
                int EL = StrHtml.IndexOf(E_Str);
                if (SL != -1 & EL != -1)
                {
                    if (SL < EL)
                    {
                        string ReplaceStr = StrHtml.Substring(SL, EL - SL + E_Str.Length);
                        StrHtml = StrHtml.Replace(ReplaceStr, "");
                        ReplaceStr = ReplaceStr.Replace(S_Str, "");
                        ReplaceStr = ReplaceStr.Replace(E_Str, "");
                        if (Result == "")
                        {
                            Result = ReplaceStr;
                        }
                        else
                        {
                            Result =Result+"<#分隔線#>"+ ReplaceStr;
                        }
                        return Get_SpecialChar(S_Str, E_Str, StrHtml, Result);
                    }
                    else
                    {
                        return Result;
                    }
                }
                else
                {
                    return Result;
                }
            }
        }
    }
}
