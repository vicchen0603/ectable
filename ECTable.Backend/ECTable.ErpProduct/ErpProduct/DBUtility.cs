﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
public class DBUtility
{
    public enum DBServer
    {
        DB
    }

    private string _ConnStr;
    private MySqlConnection _BaseConnection;
    private MySqlTransaction _BaseTransaction;
    private bool _IsOpenTransaction = false;

    private MySqlCommand _baseCommand;
    private string ConnectionString
    {
        get { return _ConnStr; }
        set { _ConnStr = value; }
    }


    public DBUtility(DBServer p_DBServer, bool p_IsOpenTransaction = false)
    {
        switch (p_DBServer)
        {
            case DBServer.DB:
                ConnectionString = ConfigurationManager.ConnectionStrings["yonglinfarmDB"].ConnectionString;
                break;
        }

        _BaseConnection = new MySqlConnection(ConnectionString);
        _IsOpenTransaction = p_IsOpenTransaction;
    }

    /// <summary>
    /// 設定共用的Command屬性
    /// </summary>
    /// <param name="p_CommandType"></param>
    /// <param name="p_CommandText"></param>
    /// <param name="p_Parameters"></param>
    /// <remarks></remarks>
    private void SetCommandObject(CommandType p_CommandType, string p_CommandText, params MySqlParameter[] p_Parameters)
    {
        try
        {            
            _baseCommand = _BaseConnection.CreateCommand();
            _baseCommand.CommandText = p_CommandText;
            _baseCommand.CommandType = p_CommandType;
            _baseCommand.CommandTimeout = 60;
            _baseCommand.Parameters.Clear();



            AddParameters(_baseCommand, p_Parameters);
        }
        catch (MySqlException oex)
        {
        }
        catch (Exception ex)
        {
        }
    }


    /// <summary>
    /// add parameters
    /// </summary>
    /// <param name="command"></param>
    /// <param name="commandParameters"></param>
    /// <remarks></remarks>
    private void AddParameters(MySqlCommand command, MySqlParameter[] commandParameters)
    {

        if (commandParameters != null)
        {
            foreach (MySqlParameter p in commandParameters)
            {
                if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }

                command.Parameters.Add(p);
            }
        }
    }

    private void CheckConnection()
    {
        if (_BaseConnection.State == ConnectionState.Closed)
        {
            _BaseConnection.Open();
        }

        if (_IsOpenTransaction)
        {
            _BaseTransaction = _BaseConnection.BeginTransaction();
        }
    }

    public void Open()
    {
        CheckConnection();
    }

    public void Commit()
    {
        if (_BaseTransaction != null)
        {
            _BaseTransaction.Commit();
            _BaseTransaction = null;
        }
    }

    public void RollBack()
    {
        if (_BaseTransaction != null)
        {
            _BaseTransaction.Rollback();
            _BaseTransaction = null;
        }
    }

    public void Close()
    {
        if (_BaseTransaction != null)
        {
            _BaseTransaction = null;
        }

        if (_BaseConnection != null)
        {
            if (_BaseConnection.State != ConnectionState.Closed)
            {
                _BaseConnection.Close();
            }

            _BaseConnection = null;
        }
    }


    /// <summary>
    /// FillDataSet
    /// </summary>
    /// <param name="p_CommandType">sql/store procedure</param>
    /// <param name="p_CommandText"></param>
    /// <param name="p_Parameters"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public DataSet FillDataSet(CommandType p_CommandType, string p_CommandText, params MySqlParameter[] p_Parameters)
    {
        try
        {
            //處理連線檢查
            CheckConnection();

            SetCommandObject(p_CommandType, p_CommandText, p_Parameters);

            MySqlDataAdapter oda = new MySqlDataAdapter(_baseCommand);

            DataSet ds = new DataSet();
            oda.Fill(ds);
            return ds;
        }
        catch (MySqlException oex)
        {
            return null;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    /// <summary>
    /// 提供insert、update、delete等更新語法的執行
    /// </summary>
    /// <param name="p_CommandType">SQL/store procedure</param>
    /// <param name="p_CommandText">SQL/store procedure name</param>
    /// <param name="p_Parameters">Parameters</param>        
    /// <returns></returns>
    /// <remarks></remarks>
    public int ExecuteNonQuery(CommandType p_CommandType, string p_CommandText, params MySqlParameter[] p_Parameters)
    {
        try
        {
            //處理連線檢查
            CheckConnection();

            SetCommandObject(p_CommandType, p_CommandText, p_Parameters);

            //設定是否開啟交易
            if (_BaseTransaction != null)
            {
                _baseCommand.Transaction = _BaseTransaction;
            }

            return _baseCommand.ExecuteNonQuery();
        }
        catch (MySqlException oex)
        {
            return 0;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public MySqlDataReader ExecuteReader(CommandType p_CommandType, string p_CommandText, params MySqlParameter[] p_Parameters)
    {
        try
        {
            CheckConnection();
            SetCommandObject(p_CommandType, p_CommandText, p_Parameters);

            return _baseCommand.ExecuteReader();
        }
        catch (MySqlException oex)
        {
            
        }
        catch (Exception ex)
        {            
        }
        return null;
    }

    public string ExecuteScalar(CommandType p_CommandType, string p_CommandText, params MySqlParameter[] p_Parameters)
    {
        try
        {
            CheckConnection();
            SetCommandObject(p_CommandType, p_CommandText, p_Parameters);

            object oj = new object();
            oj=_baseCommand.ExecuteScalar();
            if (oj != null)
            {
                return oj.ToString();
            }
            else
            {
                return "";
            }
            
        }
        catch (MySqlException oex)
        {
            return "Error";
        }
        catch (Exception ex)
        {
            return "Error";
        }
    }

}