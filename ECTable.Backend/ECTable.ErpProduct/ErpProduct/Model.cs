﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErpProduct
{
    class Model
    {

        /// <summary>
        /// 選單Code
        /// </summary>
        public Boolean is_enable { get; set; }
        /// <summary>
        /// 商品編號
        /// </summary>
        public string ProductID { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 商品價格
        /// </summary>
        public int Price { get; set; }
        /// <summary>
        /// 是否應稅
        /// </summary>
        public Boolean IsTax { get; set; }
        /// <summary>
        /// 保存方式
        /// </summary>
        public string Preservation { get; set; }
        /// <summary>
        /// 規格
        /// </summary>
        public string Standard { get; set; }
        /// <summary>
        /// 單位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 商品特色1
        /// </summary>
        public string Features1 { get; set; }
        /// <summary>
        /// 商品特色2
        /// </summary>
        public string Features2 { get; set; }
        /// <summary>
        /// 商品特色3
        /// </summary>
        public string Features3 { get; set; }
        /// <summary>
        /// 商品特色4
        /// </summary>
        public string Features4 { get; set; }
        /// <summary>
        /// 商品特色5
        /// </summary>
        public string Features5 { get; set; }
        /// <summary>
        /// 商品描述
        /// </summary>
        public string Content { get; set; }
    }
}
