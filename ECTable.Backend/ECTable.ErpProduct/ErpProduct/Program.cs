﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using NLog;

namespace ErpProduct
{
    class Program
    {

        static string path = ConfigurationSettings.AppSettings["path"].ToString();
        static string backPath = ConfigurationSettings.AppSettings["backPath"].ToString();
        static  DBUtility db = new DBUtility(DBUtility.DBServer.DB);
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        static bool isError = false;

        static void Main(string[] args)
        {
            string fileName = DateTime.Now.ToString("yyyyMMdd") + "-products.json";
            string backFileName = DateTime.Now.ToString("yyyyMMdd") + "-products.ok";
            string logFileName = DateTime.Now.ToString("yyyyMMdd") + "-products.log";
            string errFileName = DateTime.Now.ToString("yyyyMMdd") + "-products.err";
            string JsonStr = "";

            _logger.Info("============ Start =============");

            try
            {
                if (File.Exists(path + fileName) == true)
                {
                    try
                    {
                        JsonStr = File.ReadAllText(path + fileName);
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        _logger.Error(String.Format("讀取檔案時，fileName：{0}，發生錯誤：{1}", fileName, ex));
                    }
                    if (JsonStr != "" && JsonStr != "[]")
                    {
                        List<Model> _Model = GetModel(JsonStr);
                        string result = ProductProcess(_Model);
                        StreamWriter sw = new StreamWriter(path + logFileName, true);
                        sw.WriteLine(result);
                        sw.Flush();
                        sw.Close();
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(path + logFileName, true);
                        sw.WriteLine("檔案資料空白，略過不處理!");
                        _logger.Info(String.Format("檔案資料空白，略過不處理，檔案路徑：{0}", path + fileName));
                        sw.Flush();
                        sw.Close();
                    }

                    // 搬移檔案
                    if (isError) File.Move(path + fileName, backPath + errFileName);
                    else File.Move(path + fileName, backPath + backFileName);
                }
                else
                {
                    _logger.Info(String.Format("Main 執行時，檔案不存在，檔案路徑：{0}", path + fileName));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("Main 執行時，發生錯誤：{0}", ex));
            }

            _logger.Info("============ END =============");

            //Console.ReadKey(); // 保留視窗不關，debug 用
        }

        /// <summary>
        /// 寫入 Model
        /// </summary>
        /// <param name="JsonStr">json</param>
        /// <returns></returns>
        static public List<Model> GetModel(string JsonStr)
        {
            List<Model> _Model = new List<Model>();

            try
            {
                List<JObject> Product = JsonConvert.DeserializeObject<List<JObject>>(JsonStr);
                var QueryList = from ProductData in Product
                                select ProductData;

                foreach (var row in QueryList)
                {
                    // 單筆執行錯誤時，略過跳下一筆
                    try
                    {
                        string product_summary = ComUtility.StringTool.IIfCheckDBNull(row["product_summary"].ToString().Replace("\"", "'"));
                        string Features1 = "";
                        string Features2 = "";
                        string Features3 = "";
                        string Features4 = "";
                        string Features5 = "";
                        string[] strs = product_summary.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i <= strs.Length - 1; i++)
                        {
                            if (i == 0) Features1 = strs[i].ToString();
                            if (i == 1) Features2 = strs[i].ToString();
                            if (i == 2) Features3 = strs[i].ToString();
                            if (i == 3) Features4 = strs[i].ToString();
                            if (i == 4) Features5 = strs[i].ToString();
                        }
                        _Model.Add(new Model()
                        {
                            ProductID = row["product_id"].ToString(),
                            ProductName = row["product_name"].ToString(),
                            is_enable = Convert.ToBoolean(row["is_enable"].ToString()),
                            IsTax = Convert.ToBoolean(row["product_is_taxable"].ToString()),
                            Price = Convert.ToInt32(row["product_price"].ToString()),
                            Preservation = row["product_preservation_methods"].ToString(),
                            Standard = row["product_standard"].ToString(),
                            Unit = row["product_unit"].ToString(),
                            Features1 = Features1,
                            Features2 = Features2,
                            Features3 = Features3,
                            Features4 = Features4,
                            Features5 = Features5,
                            Content = row["product_summary"].ToString().Replace("\"", "'") + row["product_description"].ToString().Replace("\"", "'") // 特色與描述，合併寫入 content
                        });
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        _logger.Error(String.Format("GetModel 執行，product_id：{0}，product_name：{1}，讀取逐筆資料時，發生錯誤：{2}", row["product_id"].ToString(), row["product_name"].ToString(), ex));
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                _logger.Error(String.Format("GetModel 執行時，JsonStr：{0}，發生錯誤：{1}", JsonStr, ex));
            }

            return _Model;
        }
        static public string ProductProcess(List<Model> _Model)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                foreach (var l in _Model)
                {
                    try
                    {
                        MySqlParameter[] para = new MySqlParameter[1];
                        para[0] = new MySqlParameter("ProductID", l.ProductID);
                        string Sql = "select ID from products where ProductID=@ProductID";
                        string ID = ComUtility.StringTool.IIfCheckDBNull(db.ExecuteScalar(CommandType.Text, Sql, para)).ToString();

                        MySqlParameter[] Procpara = new MySqlParameter[14];
                        Procpara[0] = new MySqlParameter("ProductID", l.ProductID);
                        Procpara[1] = new MySqlParameter("ProductName", l.ProductName);
                        Procpara[2] = new MySqlParameter("Price", l.Price);
                        Procpara[3] = new MySqlParameter("Specification", l.Standard);
                        Procpara[4] = new MySqlParameter("Preservation", l.Preservation);
                        Procpara[5] = new MySqlParameter("Unit", l.Unit);
                        Procpara[6] = new MySqlParameter("Features1", l.Features1);
                        Procpara[7] = new MySqlParameter("Features2", l.Features2);
                        Procpara[8] = new MySqlParameter("Features3", l.Features3);
                        Procpara[9] = new MySqlParameter("Features4", l.Features4);
                        Procpara[10] = new MySqlParameter("Features5", l.Features5);
                        Procpara[11] = new MySqlParameter("Content", l.Content);
                        Procpara[12] = new MySqlParameter("IsEnable", l.is_enable);
                        Procpara[13] = new MySqlParameter("IsTax", l.IsTax);
                        int actId;
                        if (ID == "")
                        {
                            //新增
                            Sql = "insert into products(" + Environment.NewLine;
                            Sql += "ProductID,ERPProductName,ProductName,Brand_ID,Class_ID,Price,Specification,Preservation," + Environment.NewLine;
                            Sql += "Unit,SeoKeyword,SeoDesc,Features1,Features2,Features3,Features4,Features5,Content," + Environment.NewLine;
                            Sql += "IsEnable,Status,IsTax,IsBuy,DeliveryType,ErpModifyFlag,CreateID,CreateTime,UpdateID,UpdateTime,Stock,ErpStock" + Environment.NewLine;
                            Sql += ")values" + Environment.NewLine;
                            Sql += "(" + Environment.NewLine;
                            Sql += "@ProductID,@ProductName,@ProductName,0,0,@Price,@Specification,@Preservation," + Environment.NewLine;
                            Sql += "@Unit,'','',@Features1,@Features2,@Features3,@Features4,@Features5,@Content," + Environment.NewLine;
                            Sql += "@IsEnable,1,@IsTax,1,0,0,'Erp',now(),'Erp',now(),0,0" + Environment.NewLine;
                            Sql += ")" + Environment.NewLine;
                            actId = db.ExecuteNonQuery(CommandType.Text, Sql, Procpara);
                            if (actId == 0)
                            {
                                isError = true;
                                sb.AppendLine("商品編號：" + l.ProductID + "新增失敗");
                                _logger.Info(String.Format("商品編號：{0}，新增失敗", l.ProductID));
                            }
                            else
                            {
                                sb.AppendLine("商品編號：" + l.ProductID + "新增成功");
                                _logger.Info(String.Format("商品編號：{0}，新增成功", l.ProductID));
                            }
                        }
                        else
                        {
                            Sql = "update products set " + Environment.NewLine;
                            Sql += "ERPProductName=@ProductName,Price=@Price,Specification=@Specification,Preservation=@Preservation,erpModify=1" + Environment.NewLine;
                            Sql += "Unit=@Unit,Features1=@Features1,Features2=@Features2,Features3=@Features3,Features4=@Features4,Features5=@Features5," + Environment.NewLine;
                            Sql += "Content=@Content,IsEnable=@IsEnable,IsTax=@IsTax,UpdateID='Erp',UpdateTime=now()" + Environment.NewLine;
                            Sql += "where ID=" + ID + Environment.NewLine;

                            actId = db.ExecuteNonQuery(CommandType.Text, Sql, Procpara);
                            if (actId == 0)
                            {
                                isError = true;
                                sb.AppendLine("商品編號：" + l.ProductID + "編輯失敗");
                                _logger.Info(String.Format("商品編號：{0}，編輯失敗", l.ProductID));
                            }
                            else
                            {
                                sb.AppendLine("商品編號：" + l.ProductID + "編輯成功");
                                _logger.Info(String.Format("商品編號：{0}，編輯成功", l.ProductID));
                            }
                        }

                        //MySqlParameter[] para = new MySqlParameter[1];
                        //para[0] = new MySqlParameter("ProductID", l.ProductID );
                        //db.ExecuteNonQuery(CommandType.Text, Sql, para); 
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        _logger.Error(String.Format("ProductProcess 執行，商品編號：{0}，商品名稱：{1}，讀取逐筆資料時，發生錯誤：{2}", l.ProductID, l.ProductName, ex));
                    }            
                }
            }
            catch (Exception ex)
            {
                isError = true;
                _logger.Error(String.Format("ProductProcess 執行時，發生錯誤：{0}", ex));
            }

            return sb.ToString();
        }

    }
}
