﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErpStock
{
    class Model
    {

        /// <summary>
        /// 商品編號
        /// </summary>
        public string ProductID { get; set; }
        /// <summary>
        /// 庫存量
        /// </summary>
        public string Stocks { get; set; }        
    }
}
