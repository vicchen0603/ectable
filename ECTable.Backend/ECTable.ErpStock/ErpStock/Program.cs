﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using NLog;

namespace ErpStock
{
    class Program
    {
        static string path = ConfigurationSettings.AppSettings["path"].ToString();
        static string backPath = ConfigurationSettings.AppSettings["backPath"].ToString();
        static DBUtility db = new DBUtility(DBUtility.DBServer.DB);
        protected static Logger _logger = LogManager.GetCurrentClassLogger(); // NLog 宣告
        static bool isError = false;

        static void Main(string[] args)
        {
            _logger.Info("============ Start =============");

            string fileName = DateTime.Now.ToString("yyyyMMdd") + "-stocks.json";
            string backFileName = DateTime.Now.ToString("yyyyMMdd") + "-stocks.ok";
            string logFileName = DateTime.Now.ToString("yyyyMMdd") + "-stocks.log";
            string errFileName = DateTime.Now.ToString("yyyyMMdd") + "-stocks.err";
            string JsonStr = "";

            try
            {
                if (File.Exists(path + fileName) == true)
                {
                    try
                    {
                        JsonStr = File.ReadAllText(path + fileName);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(String.Format("讀取檔案時，fileName：{0}，發生錯誤：{1}", fileName, ex));
                    }
                    if (JsonStr != "" && JsonStr != "[]")
                    {
                        List<Model> _Model = GetModel(JsonStr);
                        string result = ProductStocksProcess(_Model);
                        StreamWriter sw = new StreamWriter(path + logFileName, true);
                        sw.WriteLine(result);
                        sw.Flush();
                        sw.Close();
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(path + logFileName, true);
                        sw.WriteLine("檔案資料空白，略過不處理!");
                        _logger.Info(String.Format("檔案資料空白，略過不處理，檔案路徑：{0}", path + fileName));
                        sw.Flush();
                        sw.Close();
                    }
                    
                    // 搬移檔案
                    if (isError) File.Move(path + fileName, backPath + errFileName);
                    else File.Move(path + fileName, backPath + backFileName);
                }
                else
                {
                    _logger.Info(String.Format("Main 執行時，檔案不存在，檔案路徑：{0}", path + fileName));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("Main 執行時，發生錯誤：{0}", ex));
            }

            _logger.Info("============ End =============");

            //Console.ReadKey(); // 保留視窗不關，debug 用
        }

        /// <summary>
        /// 寫入 Model
        /// </summary>
        /// <param name="JsonStr">json</param>
        /// <returns></returns>
        static public List<Model> GetModel(string JsonStr)
        {
            List<Model> _Model = new List<Model>();

            try
            {
                List<JObject> Product = JsonConvert.DeserializeObject<List<JObject>>(JsonStr);
                var QueryList = from ProductData in Product
                                select ProductData;

                foreach (var row in QueryList)
                {
                    try
                    {
                        _Model.Add(new Model()
                        {
                            ProductID = row["product_id"].ToString(),
                            Stocks = row["stock_changes"].ToString()
                        });
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        _logger.Error(String.Format("GetModel 執行，product_id：{0}，讀取逐筆資料時，發生錯誤：{1}", row["product_id"].ToString(), ex));
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                _logger.Error(String.Format("GetModel 執行時，發生錯誤：{0}", ex));
            }

            return _Model;
        }

        /// <summary>
        /// 新增/更新資料
        /// </summary>
        /// <param name="_Model"></param>
        /// <returns></returns>
        static public string ProductStocksProcess(List<Model> _Model)
        {

            StringBuilder sb = new StringBuilder();

            try
            {
                foreach (var l in _Model)
                {
                    MySqlParameter[] para = new MySqlParameter[1];

                    try
                    {
                        para[0] = new MySqlParameter("ProductID", l.ProductID);
                        string Sql = "select ID,erpstock from products where ProductID=@ProductID";
                        DataSet ds = db.FillDataSet(CommandType.Text, Sql, para);

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            MySqlParameter[] Procpara = new MySqlParameter[2];
                            Procpara[0] = new MySqlParameter("erpstock", l.Stocks);
                            Procpara[1] = new MySqlParameter("ID", ds.Tables[0].Rows[0]["ID"].ToString());

                            Sql = "update products set " + Environment.NewLine;
                            Sql += "erpstock=erpstock+@erpstock" + Environment.NewLine;
                            Sql += "where ID=" + ds.Tables[0].Rows[0]["ID"].ToString() + Environment.NewLine;
                            int actID = db.ExecuteNonQuery(CommandType.Text, Sql, Procpara);
                            if (actID == 0)
                            {
                                sb.AppendLine("商品編號：" + l.ProductID.ToString() + "庫存新增失敗");
                                _logger.Info(String.Format("商品編號：{0}，庫存新增失敗", l.ProductID.ToString()));
                            }
                            else
                            {
                                //記錄Log
                                Sql = "insert into products_stock_log(product_id,order_id,member_id,stock,uptstock,kind,content,createid,createtime)" + Environment.NewLine;
                                Sql += "values(@ID,0,0,@stock,@uptstock,0,'Erp過轉庫存','Erp',now())" + Environment.NewLine;
                                MySqlParameter[] UptProcpara = new MySqlParameter[3];
                                UptProcpara[0] = new MySqlParameter("stock", Convert.ToInt32(ds.Tables[0].Rows[0]["erpstock"].ToString()));
                                UptProcpara[1] = new MySqlParameter("uptstock", l.Stocks + Convert.ToInt32(ds.Tables[0].Rows[0]["erpstock"].ToString()));
                                UptProcpara[2] = new MySqlParameter("ID", Convert.ToInt32(ds.Tables[0].Rows[0]["ID"].ToString()));

                                int actLogId = db.ExecuteNonQuery(CommandType.Text, Sql, UptProcpara);
                                if (actLogId == 0)
                                {
                                    sb.AppendLine("商品編號：" + l.ProductID.ToString() + "庫存新增成功，但LOG記錄檔新增失敗");
                                    _logger.Info(String.Format("商品編號：{0}，庫存新增成功，但LOG記錄檔新增失敗", l.ProductID.ToString()));
                                }
                                else
                                {
                                    sb.AppendLine("商品編號：" + l.ProductID.ToString() + "庫存新增成功");
                                    _logger.Info(String.Format("商品編號：{0}，庫存新增成功", l.ProductID.ToString()));
                                }
                            }
                        }
                        else
                        {
                            sb.AppendLine("商品編號：" + l.ProductID.ToString() + "無此商品編號");
                            _logger.Info(String.Format("商品編號：{0}，無此商品編號", l.ProductID.ToString()));
                        }
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        _logger.Error(String.Format("ProductStocksProcess 執行，product_id：{0}，讀取逐筆資料，新增/更新時，發生錯誤：{1}", l.ProductID, ex));
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                _logger.Error(String.Format("GetModel 執行時，發生錯誤：{0}", ex));
            }

            return sb.ToString();
        }

    }
}
