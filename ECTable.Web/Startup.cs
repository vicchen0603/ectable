﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECTable.Web.Startup))]
namespace ECTable.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
